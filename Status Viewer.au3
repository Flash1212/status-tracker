#include <date.au3>
#include <File.au3>
#include <Array.au3>
#include <Debug.au3>
#include <String.au3>
#include <SQLite.au3>
#include <Services.au3>
#include <SQLite.dll.au3>

#include ".\miscLibrary\misc.au3"

Opt("MustDeclareVars", 1)

Global $bSync = False ; This variable is used to determine if the system is synchronization with CUCM. Will turn true during automatic sync and manual.
Global $hDB
Global Const 	$sTables[4][2] = [["built", "DName"], ["batch", "Batch"], ["status", "status"], ["devices", "DName,LKnownIP,AddDate,LKnownRegDate,LknownRegStat,CRegStat,ModDate,ModTracker"]], _
				$iBuilt = 0, $iBatch = 1, $iStatus = 2, $iDevices = 3, $iName = 0, $iColumns = 1, _
				$errorLog = ".\Logs\error.log", _
				$sDB = ".\db\Tracker.db", _
				$bVerbose = False, _
				$bError = True

_Main()

Func _Main()

EndFunc


Func _Startup($sTables)
	Local $Region = "Region - SQL Startup", $fDLLsql, $sSQL_ver, $sReturn
	_Log($Region & ",Global,SQLite Startup activated.")
	$fDLLsql = _SQLite_Startup()
	If @error Then
		If $bDEBUG Then MsgBox($MB_SYSTEMMODAL, "SQLite Error", "SQLite3 dll Can't be Loaded!")
		If $bError Then _Log($Region & ",Error @  _SQLite_Startup")
		If $bError Then _Log($Region & ",SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
		Exit -1
	EndIf
	_Log($Region & ",Global," & $fDLLsql)

	$sSQL_ver = "_SQLite_LibVersion=" & _SQLite_LibVersion()
	_Log($Region & ",Global," & $sSQL_ver)
	If Not FileExists($sDB) Then



	Else
		$hDB = _SQLite_Open($sDB)
		If @error Then
			If $bError Then _Log($Region & ",Error,Error opening DB. @error: " & @error)
			If $bError Then _Log($Region & ",Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
		Else
			_Log($Region & ",Info,Successfully opened existing DB: " & $sDB)
		EndIf

		$sReturn = _SQLite_Exec($hDB, "DELETE From batch;")
		If @error Then
			If $bError Then _Log($Region & ",Error,Error dropping batch table. @error: " & @error)
			If $bError Then _Log($Region & ",Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
		Else
			_Log($Region & ",Info,Successfully dropped batch table: " & $sDB)
		EndIf

	EndIf

EndFunc   ;==>_Startup

;Shutdown DB
; #FUNCTION# ====================================================================================================================
; Name ..........: _Shutdown
; Description ...:
; Syntax ........: _Shutdown()
; Parameters ....:
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _Shutdown()