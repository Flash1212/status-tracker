#include <date.au3>
#include <File.au3>
#include <Array.au3>
#include <Debug.au3>
#include <String.au3>
#include <SQLite.au3>
#include <Services.au3>
#include <SQLite.dll.au3>

#include ".\miscLibrary\misc.au3"

Global $bSync = False ; This variable is used to determine if the system is synchronization with CUCM. Will turn true during automatic sync and manual.
Global $hDB
Global Const 	$aTables[4][2] = [["built", "DName"], ["batch", "Batch"], ["status", "status"], ["devices", "DName,LKnownIP,AddDate,LKnownRegDate,LknownRegStat,CRegStat,ModDate,ModTracker"]], _
				$sDName = 'DName', $sLKnownIP = 'LKnownIP', $sAddDate = 'AddDate', $sLKnownRegDate = 'LKnownRegDate', $sLknownRegStat = 'LknownRegStat', $sCRegStat = 'CRegStat', _
				$sModDate = 'ModDate', $sModTracker = 'ModTracker', _
				$sRegistered = 'Registered', $sUnRegistered = 'UnRegistered', $sPartiallyRegistered = 'PartiallyRegistered', $sUnknown = 'Unknown', $sRejected = 'Rejected',  _
				$aStatus = [$sRegistered,$sUnRegistered,$sPartiallyRegistered, $sRejected, $sUnknown], _
				$iBuilt = 0, $iBatch = 1, $iStatus = 2, $iDevices = 3, $iName = 0, $iColumns = 1, _
				$errorLog = ".\Logs\error.log", _
				$sDB = ".\db\Tracker.db", _
				$bVerbose = False, _
				$bError = True


Opt("MustDeclareVars", 1)

_Main()

Func _Main()
	Local	$sTable = $aTables[$iDevices][$iName], $sNdex = 'CRegStat', $sType = ""
	_Startup()
;~ #cs
	For $sStatus in $aStatus
		_Show_Report($sTable, Default, Default, $sStatus)
	Next
;~ #ce

;~ 	_Report($sTable, $sNdex, '')
	_Shutdown()
EndFunc

Func _Startup()
	Local $Region = "Region - SQL Startup", $fDLLsql, $sSQL_ver, $sReturn
	_Log($Region & ",Global,SQLite Startup activated.")
	$fDLLsql = _SQLite_Startup()
	If @error Then
		If $bError Then MsgBox($MB_SYSTEMMODAL, "SQLite Error", "SQLite3 dll Can't be Loaded!")
		If $bError Then _Log($Region & ",Error @  _SQLite_Startup")
		If $bError Then _Log($Region & ",SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
		Exit -1
	EndIf
	_Log($Region & ",Global," & $fDLLsql)

	$sSQL_ver = "_SQLite_LibVersion=" & _SQLite_LibVersion()
	_Log($Region & ",Global," & $sSQL_ver)
	If Not FileExists($sDB) Then

		_Log($Region & ",Error, No DB found. Terminating system.")
		MsgBox(64, "Error", "No DB found."&@CRLF&"System terminating")
		_Shutdown()
	Else
		$hDB = _SQLite_Open($sDB)
		If @error Then
			If $bError Then _Log($Region & ",Error,Error opening DB. @error: " & @error)
			If $bError Then _Log($Region & ",Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
		Else
			_Log($Region & ",Info,Successfully opened existing DB: " & $sDB)
		EndIf

		$sReturn = _SQLite_Exec($hDB, "DELETE From batch;")
		If @error Then
			If $bError Then _Log($Region & ",Error,Error dropping batch table. @error: " & @error)
			If $bError Then _Log($Region & ",Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
		Else
			_Log($Region & ",Info,Successfully dropped batch table: " & $sDB)
		EndIf

	EndIf

EndFunc   ;==>_Startup

;Shutdown DB
; #FUNCTION# ====================================================================================================================
; Name ..........: _Shutdown
; Description ...:
; Syntax ........: _Shutdown()
; Parameters ....:
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _Shutdown()
	Local $Region = "Region - SQL Shutdown"
	_Log($Region & ", Global,Closing DB.")
	_SQLite_Close()

	_Log($Region & ", Global,Shutting down SQLite.")
	_SQLite_Shutdown()

	_Log($Region & ", Global, Exiting system.")
	Exit
EndFunc   ;==>_Shutdown


; #FUNCTION# ====================================================================================================================
; Name ..........: _Show_Report
; Description ...:
; Syntax ........: _Show_Report($sTable, $sNdex[, $sType = ''[, $sStatus = ''[, $aTable = '', $aTemp, $aInstances]]])
; Parameters ....: $sTable              - a string value. "devices", "mods"
;                  $sNdex               - a string value. "DName", "LKnownIP", "AddDate", "LKnownRegDate", "LKnownRegStat", "CRegStat", "Epoch", "Instance"
;                  $sType               - [optional] a string value. Default is ''. "" = All; else is "time", "name", "purge", "both"
;                  $sStatus             - [optional] a string value. Default is ''.
;                  $aTable              - [optional] an array of unknowns. Default is ''.
;                  $aTemp               - an array of unknowns.
;                  $aInstances          - an array of unknowns.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _Show_Report($sTable, $sType = '', $sNdex = '', $sStatus = '', $sName = '', $sTimeBefore = '', $sTimeAfter = '', $sTimeBetween = '')
	Local	$iRval, $aResult, $iRows, $iColumns, $aTemp, $aTime

	;Determin if sName is partial or complete if even used.
	If $sType = 'name' or $sType = 'both' Then
		If StringLen($sName) < 15 Then $sName = $sName&"%"
	EndIf

	If $sType = 'time' or $sType = 'both'  Then
		If $sTimeBetween <> '' Then $aTime = StringSplit($sTimeBetween, '|')
	EndIf


	Switch $sTable
		Case  'devices'
			Switch	$sType
				Case	'' ; Nothing special just a generic report
					Switch $sStatus
						Case	'' ; All
							$iRval = _SQLite_GetTable2d(-1, "SELECT * FROM "&$sTable&";", $aResult, $iRows, $iColumns)
						Case 	Else
							$iRval = _SQLite_GetTable2d(-1, "SELECT * FROM "&$sTable&" WHERE "&$sNdex&"='"&$sStatus&"';", $aResult, $iRows, $iColumns)
					EndSwitch
				Case	Else
					Switch $sType
						Case	'time' ;4 options here; Before,
							Switch $sStatus
								Case	'' ; Generic time based report
									$iRval = _SQLite_GetTable2d(-1, "SELECT * FROM "&$sTable&";", $aResult, $iRows, $iColumns)
								Case 	Else
									$iRval = _SQLite_GetTable2d(-1, "SELECT * FROM "&$sTable&" WHERE "&$sNdex&"='"&$sStatus&"';", $aResult, $iRows, $iColumns)
							EndSwitch
							;Figure out time now...


						Case 	'name' ;Unlike time we can use SQL to our advantage regarding name using 'LIKE'
							Switch $sStatus
								Case	'' ; Generic name based report
									$iRval = _SQLite_GetTable2d(-1, "SELECT * FROM "&$sTable&" WHERE DName LIKE "&$sName, $aResult, $iRows, $iColumns)
								Case 	Else
									$iRval = _SQLite_GetTable2d(-1, "SELECT * FROM "&$sTable&" WHERE DName LIKE "&$sName& " AND " &$sNdex&"='"&$sStatus&"';", $aResult, $iRows, $iColumns)
							EndSwitch
					EndSwitch
			EndSwitch
		Case 'mods'

	EndSwitch

	If $iRval = $SQLITE_OK Then
		ConsoleWrite("Rows: "&$iRows&@CRLF&"Columns: "&$iColumns&@CRLF)
	;	_SQLite_Display2DResult($aResult, 'Results')
		_DebugArrayDisplay($aResult, $sStatus)
	Else
		MsgBox($MB_SYSTEMMODAL, "SQLite Error: " & $iRval, _SQLite_ErrMsg())
	EndIf

EndFunc

Func _Time_Report()

EndFunc

Func _Name_Report()

EndFunc

Func _Purge_Report()

EndFunc

#cs
Func _Search_Array(ByRef $aTable, $sStatus)
	Local	$i_Search = 1, $i_Start = 0, $aInstances
	_ArraySort($aTable, Default, Default, Default, 5)

	While $i_SC = 1

		_ArrayBinarySearch($aTable, $sStatus, $i_Start, 0, 5)

	WEnd

EndFunc
#ce

Func _Export_Report()

EndFunc

Func _Tracker_Status()

EndFunc

