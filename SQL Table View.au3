#include <MsgBoxConstants.au3>
#include <Debug.au3>
#include <SQLite.au3>
#include <SQLite.dll.au3>

Local $aResult, $iRows, $iColumns, $iRval, $sDB = '.\db\Tracker.db', $aTables, $aTables2, $iR, $iC

_SQLite_Startup()
If @error Then
	MsgBox($MB_SYSTEMMODAL, "SQLite Error", "SQLite.dll Can't be Loaded!")
	Exit -1
EndIf

ConsoleWrite("_SQLite_LibVersion=" & _SQLite_LibVersion() & @CRLF)

_SQLite_Open($sDB) ; Open a :memory: database
If @error Then
	MsgBox($MB_SYSTEMMODAL, "SQLite Error", "Can't Load Database!")
	Exit -1
EndIf

_SQLite_GetTable(-1, "select tbl_name from sqlite_master where type = 'table' order by ""Table"";", $aTables, $iRows, $iColumns)
If @error And _SQLite_ErrCode() <> 0 Then
	ConsoleWrite(" Error, SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
EndIf

_ArrayDelete($aTables, '0-1')

;~ _DebugArrayDisplay($aTables, 'Table')


For $sTable In $aTables
;~ #cs
	$iRval = _SQLite_GetTable2d(-1, "PRAGMA TABLE_INFO("&$sTable&");", $aTables2, $iR, $iC)
	If $iRval = $SQLITE_OK Then
;~ 		_SQLite_Display2DResult($aResult, 'Results')
;~ 		_DebugArrayDisplay($aTables2, $table)

	Else
		MsgBox($MB_SYSTEMMODAL, "SQLite Error: " & $iRval, _SQLite_ErrMsg())
		Exit
	EndIf
;~ #ce
	;$sReturn = _SQLite_GetTable2d(-1, "SELECT * FROM "&$table&";", $aResult, $iRows, $iColumns)
	$sReturn = _SQLite_GetTable2d(-1, "SELECT * FROM "&$sTable&";", $aResult, $iRows, $iColumns)
	If $iRval = $SQLITE_OK Then
;~ 		_SQLite_Display2DResult($aResult, 'Results')
		_DebugArrayDisplay($aResult, $sTable)

	Else
		MsgBox($MB_SYSTEMMODAL, "SQLite Error: " & $iRval, _SQLite_ErrMsg())
		Exit
	EndIf

Next

#cs
$sTable = 'devices'
;~ $iRval = _SQLite_GetTable2d(-1, "SELECT * FROM "&$sTable&";", $aResult, $iRows, $iColumns)

$sNdex = 'CRegStat'
$sStatus = 'Registered'
$iRval = _SQLite_GetTable2d(-1, "SELECT * FROM "&$sTable&" WHERE "&$sNdex&"='"&$sStatus&"';", $aResult, $iRows, $iColumns)

#ce
If $iRval = $SQLITE_OK Then
;~ 		_SQLite_Display2DResult($aResult, 'Results')
	_DebugArrayDisplay($aResult, $sTable)

Else
	MsgBox($MB_SYSTEMMODAL, "SQLite Error: " & $iRval, _SQLite_ErrMsg())
	Exit
EndIf



_SQLite_Close()
_SQLite_Shutdown()