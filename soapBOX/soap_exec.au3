
#include-once
#include <.\Base64v2.au3>
#include "..\miscLibrary\misc.au3"

Global $bSHOW1, $bSHOW2, $oMyError, $g_eventerror = 0

$oMyError = ObjEvent("AutoIt.Error","_MyErrFunc")

;Ver = Version of CUCM to Call
;auth = authorization in the form of base64encode


Func _SOAP_Open($ver, $auth, $host, $postAddress, $SOAP_Action, $strEnvelope)
	Local	$strReturn, $returnText, $returnXML, $objXMLSend, $objHTTP, $objReturn, $action, _
			$returnXML, $localDebug = False
	_Log("SOAP Open activated")

	If StringInStr($SOAP_Action, 'resetPhone') then $localDebug = True

;~ 	MsgBox(64,'Debug', 'Ver: '&$ver&@CRLF&'Auth: '&$auth&@CRLF&'Post Address: '&$postAddress&@CRLF&'SOAP Action: '&$SOAP_Action&@CRLF&'Envelope: '&$strEnvelope)


	$objHTTP = ObjCreate("Msxml2.ServerXMLHTTP.6.0")
	$objXMLSend = ObjCreate("Msxml2.DOMDocument.6.0")
	$objReturn = ObjCreate("Msxml2.DOMdocument.6.0")


	$objHTTP.setOption(2, $objHTTP.GetOption(2)&" - SXH_SERVER_CERT_IGNORE_ALL_SERVER_ERRORS")

	If $bSHOW1 Then _Log($host&@CRLF)
	If $bSHOW1 Then _Log($strEnvelope&@CRLF)

	$objXMLSend.loadXML($strEnvelope)

	$objHTTP.open ("POST", $postAddress, False)

	;Set encoding
	$objHTTP.setRequestHeader ("Accept-Encoding", "gzip,deflate")

	;Set useragent
	$objHTTP.setRequestHeader ("User-Agent", "AutoIT Script")

	; Set a standard SOAP/ XML header for the content-type
	$objHTTP.setRequestHeader ("Content-Type", "text/xml;charset=UTF-8")

	; Set a header for the method to be called
	$objHTTP.setRequestHeader ("SOAPAction", $SOAP_Action )

	;Set length
	$objHTTP.setRequestHeader("Content-Length", StringLen($strEnvelope))
	_Log(StringLen($strEnvelope)&@CRLF)
	; Host
	$objHTTP.setRequestHeader("Host", $host)

	;Connection Type
	$objHTTP.setRequestHeader("Connection", "Keep-Alive")
	;Set authorization
	$objHTTP.setRequestHeader("Authorization","Basic "&_Base64Encode($auth))
	_Log(_Base64Encode($auth)&@CRLF)

	$objHTTP.send($objXMLSend)

	_Log("SOAP Open request sent")

	$strReturn = $objHTTP.responseText
	_Log("SOAP Open response received")

	$objReturn.loadXML($strReturn)

;Convert to XML
	$returnXML = $objReturn.XML
	If $bSHOW2 Then _Log($returnXML)

	;Convert to Text
;~ 	$returnText = $objReturn.Text
	;Cleanup
	$objHTTP = ''
	Return $returnXML
EndFunc

Func _MyErrFunc()

  Msgbox(0,"AutoItCOM Test","We intercepted a COM Error !"      & @CRLF  & @CRLF & _
             "err.description is: "    & @TAB & $oMyError.description    & @CRLF & _
             "err.windescription:"     & @TAB & $oMyError.windescription & @CRLF & _
             "err.number is: "         & @TAB & hex($oMyError.number,8)  & @CRLF & _
             "err.lastdllerror is: "   & @TAB & $oMyError.lastdllerror   & @CRLF & _
             "err.scriptline is: "     & @TAB & $oMyError.scriptline     & @CRLF & _
             "err.source is: "         & @TAB & $oMyError.source         & @CRLF & _
             "err.helpfile is: "       & @TAB & $oMyError.helpfile       & @CRLF & _
             "err.helpcontext is: "    & @TAB & $oMyError.helpcontext _
            )

    Local $err = $oMyError.number
    If $err = 0 Then $err = -1

    $g_eventerror = $err  ; to check for after this function returns
Endfunc

