#cs ----------------------------------------------------------------------------

	AutoIt Version: 3.3.14.5
	Author:         Dominique Thornton

	Script Function: Provide XMLs for SOAP queries
	Template AutoIt script.

#ce ----------------------------------------------------------------------------
#include-once
;#include "..\miscLibrary\misc.au3"

Opt('MustDeclareVars', 1)



; ****************************************
; Create an XSL stylesheet for transformation.
; ****************************************

Global Const $strStylesheet = "<xsl:stylesheet version=""1.0"" xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"">" & _
		"<xsl:output method=""xml"" indent=""yes""/>" & _
		"<xsl:template match=""/"">" & _
		"<xsl:copy-of select="".""/>" & _
		"</xsl:template>" & _
		"</xsl:stylesheet>"

#Region
Global $aMethod[20][2]


;10.x
$aMethod[0][1] = 'listDevicePool'
$aMethod[1][1] = 'listPhone'
$aMethod[2][1] = 'getPhoneTemplate'
$aMethod[3][1] = 'getPhone' ;all
$aMethod[4][1] = 'addPhone'
$aMethod[5][1] = 'removePhone'
$aMethod[6][1] = 'listline'
$aMethod[7][1] = 'addRoutePattern'
$aMethod[8][1] = 'executeSQLQuery'
$aMethod[9][1] = 'addLine'
$aMethod[10][1] = 'SelectCmDevice'
$aMethod[11][1] = 'updateRoutePattern'
$aMethod[12][1] = 'listRoutePartition'
$aMethod[13][1] = 'updatePhone'
$aMethod[14][1] = 'updateLine' ; This is currently onlly configured for UUIDs
$aMethod[15][1] = 'resetPhone' ; This is currently onlly configured for UUIDs
$aMethod[16][1] = 'getRoutePartition' ; This is currently onlly configured for UUIDs
$aMethod[17][1] = 'getLine' ;
$aMethod[18][1] = 'listRoutePattern' ;
$aMethod[19][1] = 'listCallManager' ;
#EndRegion
; ****************************************
; Transform the XML.
; ****************************************

; Script Start - Add your code below here
; #FUNCTION# ====================================================================================================================
; Name ..........: _XML_Formatter
; Description ...:
; Syntax ........: _XML_Formatter($sVersion, $method[, $value100 = ''[, $value101 = ''[, $method_Specific = ''[, $Desc = 'UCM10']]]])
; Parameters ....: $sVersion             - A variant value.
;                  $method              - An unknown value.
;                  $value100            - [optional] A variant value. Default is ''.
;                  $value101            - [optional] A variant value. Default is ''.
;                  $method_Specific     - [optional] An unknown value. Default is ''.
;                  $Desc                - [optional] An unknown value. Default is 'UCM10'.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _XML_Formatter($sVersion, $sHost, $sAuth, $method, $value100 = '', $value101 = '', $method_Specific = '', $Desc = 'UCM10')
;~ 	MsgBox(64,'Formatter', 'Version: '&$sVersion&@CRLF&'Method: '&$method&@CRLF&'Value: '&$value100)

	Local $aXML[6], _
			$sVer, _
			$postAddress, _
			$SOAP_Action, _
			$strEnvelope, _
			$CSS_Desc, $CSS_Name
			;$sAuth, _
			;$sHost, _

	Switch $sVersion
		Case "10"
			Select
				Case $method = 'listDevicePool'
					$sVer = "10.0"
					$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
					$SOAP_Action = "CUCM:DB ver=10 " & $method
					$strEnvelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.0">' & @CR & _
							'<soapenv:Header/>' & @CR & _
							'<soapenv:Body>' & @CR & _
							'<ns:listDevicePool sequence="?">' & @CR & _
							'<searchCriteria>' & @CR & _
							'<name>' & $value100 & '</name>' & @CR & _
							'</searchCriteria>' & @CR & _
							'<returnedTags uuid="?">' & @CR & _
							'<name>?</name>' & @CR & _
							'</returnedTags>' & @CR & _
							'</ns:listDevicePool>' & @CR & _
							'</soapenv:Body>' & @CR & _
							'</soapenv:Envelope>'
				Case $method = 'listPhone'
					$sVer = "10.0"
					$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
					$SOAP_Action = "CUCM:DB ver=10 " & $method
					$strEnvelope = 	'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.5">' & @CR & _
									'<soapenv:Header/>' & @CR & _
										'<soapenv:Body>' & @CR & _
											'<ns:' & $method & ' sequence="?">' & @CR & _
												'<searchCriteria>' & @CR & _
													'<name>' & $value100 & '</name>' & @CR & _
												'</searchCriteria>' & @CR & _
												'<returnedTags ctiid="?" uuid="?">' & @CR & _
													'<name>?</name>' & @CR & _
												'</returnedTags>' & @CR & _
											'</ns:' & $method & '>' & @CR & _
										'</soapenv:Body>' & @CR & _
									'</soapenv:Envelope>'
				Case $method = 'addPhone'
					$sVer = "10.0"
					$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
					$SOAP_Action = "CUCM:DB ver=10 " & $method
					$strEnvelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.0">' & @CR & _
							'<soapenv:Header/>' & @CR & _
							'<soapenv:Body>' & @CR & _
							'<ns:addPhone sequence="?">' & @CR & _
							'<phone ctiid="?">' & @CR & _
							'<name>' & $value100 & '</name>' & @CR & _
							'<description>?</description>' & @CR & _
							'<product>?</product>' & @CR & _
							'<class>?</class>' & @CR & _
							'<protocol>?</protocol>' & @CR & _
							'<protocolSide>?</protocolSide>' & @CR & _
							'<callingSearchSpaceName>?</callingSearchSpaceName>' & @CR & _
							'<devicePoolName>?</devicePoolName>' & @CR & _
							'<commonDeviceConfigName>?</commonDeviceConfigName>' & @CR & _
							'<commonPhoneConfigName>?</commonPhoneConfigName>' & @CR & _
							'<networkLocation>Use System Default</networkLocation>' & @CR & _
							'<locationName>?</locationName>' & @CR & _
							'<mediaResourceListName>?</mediaResourceListName>' & @CR & _
							'<securityProfileName>?</securityProfileName>' & @CR & _
							'<useDevicePoolCgpnTransformCss>?</useDevicePoolCgpnTransformCss>' & @CR & _
							'<userLocale>?</userLocale>' & @CR & _
							'<networkLocale>?</networkLocale>' & @CR & _
							'<softkeyTemplateName>?</softkeyTemplateName>' & @CR & _
							'<networkHoldMohAudioSourceId>?</networkHoldMohAudioSourceId>' & @CR & _
							'<userHoldMohAudioSourceId>?</userHoldMohAudioSourceId>' & @CR & _
							'</phone>' & @CR & _
							'</ns:addPhone>' & @CR & _
							'</soapenv:Body>' & @CR & _
							'</soapenv:Envelope>'
				Case $method = 'addDevicePool'
					$sVer = "10.0"
					$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
					$SOAP_Action = "CUCM:DB ver=10 " & $method
					$strEnvelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.0">' & @CR & _
							'<soapenv:Header/>' & @CR & _
							'<soapenv:Body>' & @CR & _
							'<ns:addDevicePool sequence="?">' & @CR & _
							'<devicePool>' & @CR & _
							'<name>' & $value100 & '</name>' & @CR & _
							'<dateTimeSettingName>?</dateTimeSettingName>' & @CR & _
							'<callManagerGroupName>?</callManagerGroupName>' & @CR & _
							'<mediaResourceListName>?</mediaResourceListName>' & @CR & _
							'<regionName >?</regionName>' & @CR & _
							'<networkLocale>?</networkLocale>' & @CR & _
							'<srstName>?</srstName>' & @CR & _
							'<connectionMonitorDuration>-1</connectionMonitorDuration>' & @CR & _
							'<locationName>?</locationName>' & @CR & _
							'<physicalLocationName uuid="?">?</physicalLocationName>' & @CR & _
							'<revertPriority>Default</revertPriority>' & @CR & _
							'</devicePool>' & @CR & _
							'</ns:addDevicePool>' & @CR & _
							'</soapenv:Body>' & @CR & _
							'</soapenv:Envelope>'
				Case $method = 'addCSS'
					$sVer = "10.0"
					$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
					$SOAP_Action = "CUCM:DB ver=10 " & $method
					$strEnvelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.0">' & @CR & _
							'<soapenv:Header/>' & @CR & _
							'<soapenv:Body>' & @CR & _
							'<ns:addCss sequence="?">' & @CR & _
							'<css>' & @CR & _
							'<description>' & $CSS_Desc & '</description>' & @CR & _
							'<members>' & @CR & _
							'<!--Zero or more repetitions:-->' & @CR & _
							'<member>' & @CR & _
							'<routePartitionName>' & $CSS_Name & '</routePartitionName>' & @CR & _
							'<index>1</index>' & @CR & _
							'</member>' & @CR & _
							'<member>' & @CR & _
							'<routePartitionName>' & $CSS_Name & '</routePartitionName>' & @CR & _
							'<index>2</index>' & @CR & _
							'</member>' & @CR & _
							'</members>' & @CR & _
							'<partitionUsage>General</partitionUsage>' & @CR & _
							'<name>' & $CSS_Name & '</name>' & @CR & _
							'</css>' & @CR & _
							'</ns:addCss>' & @CR & _
							'</soapenv:Body>' & @CR & _
							'</soapenv:Envelope>'
				Case $method = 'addRoutePattern'
					$sVer = "10.0"
					$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
					$SOAP_Action = "CUCM:DB ver=10 " & $method
					$strEnvelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.0">' & @CR & _
							'<soapenv:Header/>' & @CR & _
							'<soapenv:Body>' & @CR & _
							'<ns:addRoutePattern sequence="?">' & @CR & _
							'<routePattern>' & @CR & _
							'<pattern>' & $value100 & '</pattern>' & @CR & _
							'<description>UMCB Nortel Voicemail</description>' & @CR & _
							'<routePartitionName>voip-5digit</routePartitionName>' & @CR & _
							'<usage>Route</usage>' & @CR & _
							'<blockEnable>false</blockEnable>' & @CR & _
							'<useCallingPartyPhoneMask>Off</useCallingPartyPhoneMask>' & @CR & _
							'<networkLocation>OffNet</networkLocation>' & @CR & _
							'<patternUrgency>false</patternUrgency>' & @CR & _
							'<callingLinePresentationBit>Default</callingLinePresentationBit>' & @CR & _
							'<callingNamePresentationBit>Default</callingNamePresentationBit>' & @CR & _
							'<connectedLinePresentationBit>Default</connectedLinePresentationBit>' & @CR & _
							'<connectedNamePresentationBit>Default</connectedNamePresentationBit>' & @CR & _
							'<supportOverlapSending>false</supportOverlapSending>' & @CR & _
							'<patternPrecedence>Default</patternPrecedence>' & @CR & _
							'<releaseClause>No Error</releaseClause>' & @CR & _
							'<allowDeviceOverride>false</allowDeviceOverride>' & @CR & _
							'<provideOutsideDialtone>false</provideOutsideDialtone>' & @CR & _
							'<callingPartyNumberingPlan>Cisco CallManager</callingPartyNumberingPlan>' & @CR & _
							'<callingPartyNumberType>Cisco CallManager</callingPartyNumberType>' & @CR & _
							'<calledPartyNumberingPlan>Cisco CallManager</calledPartyNumberingPlan>' & @CR & _
							'<calledPartyNumberType>Cisco CallManager</calledPartyNumberType>' & @CR & _
							'<destination>' & @CR & _
							'<routeListName>UMCB</routeListName>' & @CR & _
							'</destination>' & @CR & _
							'<authorizationCodeRequired>false</authorizationCodeRequired>' & @CR & _
							'<authorizationLevelRequired>0</authorizationLevelRequired>' & @CR & _
							'<routeClass>Default</routeClass>' & @CR & _
							'</routePattern>' & @CR & _
							'</ns:addRoutePattern>' & @CR & _
							'</soapenv:Body>' & @CR & _
							'</soapenv:Envelope>'
				Case $method = 'getPhone'
					$sVer = "10.0"
					$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
					$SOAP_Action = "CUCM:DB ver=10 " & $method
					$strEnvelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.0">' & @CR & _
							'<soapenv:Header/>' & @CR & _
							'<soapenv:Body>' & @CR & _
							'<ns:getPhone sequence="?">' & @CR & _
							'<name>' & $value100 & '</name>' & @CR & _
							'</ns:getPhone>' & @CR & _
							'</soapenv:Body>' & @CR & _
							'</soapenv:Envelope>'


				Case $method = 'removePhone'
					$sVer = "10.0"
					$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
					$SOAP_Action = "CUCM:DB ver=10 " & $method
					$strEnvelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.0">' & @CR & _
							'<soapenv:Header/>' & @CR & _
							'<soapenv:Body>' & @CR & _
							'<ns:removePhone sequence="?">' & @CR & _
							'<name>' & $value100 & '</name>' & @CR & _
							'</ns:removePhone>' & @CR & _
							'</soapenv:Body>' & @CR & _
							'</soapenv:Envelope>'
				Case $method = 'executeSQLQuery'
					$sVer = "10.0"
					$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
					$SOAP_Action = "CUCM:DB ver=10 " & $method
					$strEnvelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.0">' & @CR & _
							'<soapenv:Header/>' & @CR & _
							'<soapenv:Body>' & @CR & _
							'<ns:executeSQLQuery sequence="?">' & @CR & _
							'<sql>' & $value100 & '</sql>' & @CR & _
							'</ns:executeSQLQuery>' & @CR & _
							'</soapenv:Body>' & @CR & _
							'</soapenv:Envelope>'
				Case $method = 'listLine'
					$sVer = "10.0"
					$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
					$SOAP_Action = "CUCM:DB ver=10 " & $method
					$strEnvelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.0">' & @CR & _
							'<soapenv:Header/>' & @CR & _
							'<soapenv:Body>' & @CR & _
							'<ns:listLine sequence="?">' & @CR & _
							'<searchCriteria>' & @CR & _
							'<pattern>' & $value100 & '</pattern>' & @CR & _
							'</searchCriteria>' & @CR & _
							'<returnedTags uuid="?">' & @CR & _
							'<pattern>?</pattern>' & @CR & _
							'<description>?</description>' & @CR & _
							'<usage>?</usage>' & @CR & _
							'<routePartitionName uuid="?">?</routePartitionName>' & @CR & _
							'<associatedDevices>' & @CR & _
							'<device>?</device>' & @CR & _
							'</associatedDevices>' & @CR & _
							'</returnedTags>' & @CR & _
							'</ns:listLine>' & @CR & _
							'</soapenv:Body>' & @CR & _
							'</soapenv:Envelope>'
				Case $method = 'addLine'
					$sVer = "10.0"
					$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
					$SOAP_Action = "CUCM:DB ver=10 " & $method
					$strEnvelope = '' ; Can't be prefilled. Has to be built realtime. See Line Compare.au3 script.
				Case $method = 'updateRoutePattern'
					$sVer = "10.0"
					$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
					$SOAP_Action = "CUCM:DB ver=10 " & $method
					$strEnvelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.0">' & @CR & _
							'<soapenv:Header/>' & @CR & _
							'<soapenv:Body>' & @CR & _
							'<ns:updateRoutePattern sequence="?">' & @CR & _
							'<pattern>' & $value100 & '</pattern>' & @CR & _
							'<routePartitionName uuid="?">voip-5digit</routePartitionName>' & @CR & _
							'<newRoutePartitionName uuid="?">UCM10-Temporary</newRoutePartitionName>' & @CR & _
							'</ns:updateRoutePattern>' & @CR & _
							'</soapenv:Body>' & @CR & _
							'</soapenv:Envelope>'
				Case $method = 'listRoutePartition'
					$sVer = "10.0"
					$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
					$SOAP_Action = "CUCM:DB ver=10 " & $method
					$strEnvelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.0">' & @CR & _
							'<soapenv:Header/>' & @CR & _
							'<soapenv:Body>' & @CR & _
							'<ns:listRoutePartition sequence="?">' & @CR & _
							'<searchCriteria>' & @CR & _
							'<name>' & $value100 & '</name>' & @CR & _
							'</searchCriteria>' & @CR & _
							'<returnedTags uuid="?">' & @CR & _
							'<name>?</name>' & @CR & _
							'</returnedTags>' & @CR & _
							'</ns:listRoutePartition>' & @CR & _
							'</soapenv:Body>' & @CR & _
							'</soapenv:Envelope>'
				Case $method = 'updatePhone'
					$sVer = "10.0"
					$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
					$SOAP_Action = "CUCM:DB ver=10 " & $method
					$strEnvelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.0">' & @CR & _
							'<soapenv:Header/>' & @CR & _
							'<soapenv:Body>' & @CR & _
							'<ns:updatePhone sequence="?">' & @CR & _
							'<name>' & $value100 & '</name>' & @CR & _
							'<' & $method_Specific & '>' & $value101 & '</' & $method_Specific & '>' & @CR & _
							'</ns:updatePhone>' & @CR & _
							'</soapenv:Body>' & @CR & _
							'</soapenv:Envelope>'
			#cs Case $method = 'updatePhone -Full Line Add'
					 $sVer = "10.0"
					 $postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
					 $SOAP_Action = "CUCM:DB ver=10 " & $method
					 $strEnvelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.0">'&@CR& _
								   '<soapenv:Header/>'&@CR& _
								   '<soapenv:Body>'&@CR& _
									  '<ns:updatePhone sequence="?">'&@CR& _
										 '<name>'&$phone&'</name>'&@CR& _
										 '<addLines>'&@CR& _
											'<line ctiid="?">'&@CR& _
											   '<index>'&$pattern_iNdex&'</index>'&@CR& _
											   '<display>'&$description&'</display>'&@CR& _
											   '<dirn>'&@CR& _
												  '<pattern>'&$pattern&'</pattern>'&@CR& _
												  '<routePartitionName>'&$partition&'</routePartitionName>'&@CR& _
											   '</dirn>'&@CR& _
											   '<ringSetting>Ring</ringSetting>'&@CR& _
											   '<consecutiveRingSetting>Use System Default</consecutiveRingSetting>'&@CR& _
											   '<ringSettingIdlePickupAlert>Use System Default</ringSettingIdlePickupAlert>'&@CR& _
											   '<ringSettingActivePickupAlert>Use System Default</ringSettingActivePickupAlert>'&@CR& _
											   '<displayAscii>'&$description&'</displayAscii>'&@CR& _
											   '<e164Mask>'&$mask&'</e164Mask>'&@CR& _
											   '<mwlPolicy>Use System Policy</mwlPolicy>'&@CR& _
											   '<maxNumCalls>'&$max&'</maxNumCalls>'&@CR& _
											   '<busyTrigger>'&$busy&'</busyTrigger>'&@CR& _
											   '<recordingFlag>Call Recording Disabled</recordingFlag>'&@CR& _
											   '<audibleMwi>Default</audibleMwi>'&@CR& _
											   '<partitionUsage>General</partitionUsage>'&@CR& _
											   '<missedCallLogging>true</missedCallLogging>'&@CR& _
											   '<recordingMediaSource>Gateway Preferred</recordingMediaSource>'&@CR& _
											'</line>'&@CR& _
										 '</addLines>'&@CR& _
									   '</ns:updatePhone>'&@CR& _
								   '</soapenv:Body>'&@CR& _
							'</soapenv:Envelope>'
					#ce
				Case $method = 'updateLine'
					$sVer = "10.0"
					$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
					$SOAP_Action = "CUCM:DB ver=10 " & $method
					$strEnvelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.0">' & @CR & _
							'<soapenv:Header/>' & @CR & _
							'<soapenv:Body>' & @CR & _
							'<ns:updateLine sequence="?">' & @CR & _
							'<' & $method_Specific & '>' & $value100 & '</' & $method_Specific & '>' & @CR & _
							'<newRoutePartitionName uuid="?">' & $value101 & '</newRoutePartitionName>' & @CR & _
							'<active>true</active>' & @CR & _
							'</ns:updateLine>' & @CR & _
							'</soapenv:Body>' & @CR & _
							'</soapenv:Envelope>'
				Case $method = 'resetPhone'
					$sVer = "10.0"
					$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
					$SOAP_Action = "CUCM:DB ver=10.0 " & $method
					$strEnvelope = 	'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.0">'&@CR& _
									   '<soapenv:Header/>'& @LF & _
									   '<soapenv:Body>'& @LF & _
										  '<ns:resetPhone sequence="1">'& @LF & _
												'<name>' & $value100 & '</name>'& @LF & _
										  '</ns:resetPhone>'& @LF & _
									   '</soapenv:Body>'& @LF & _
									'</soapenv:Envelope>'
				Case $method = 'getRoutePartition'
					$sVer = "10.0"
					$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
					$SOAP_Action = "CUCM:DB ver=10 " & $method
					$strEnvelope = 	'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.0">' & @CR & _
							'<soapenv:Header/>' & @CR & _
							'<soapenv:Body>' & @CR & _
							'<ns:getRoutePartition sequence="?">' & @CR & _
							'<uuid>' & $value100 & '</uuid>' & @CR & _
							'<returnedTags uuid="?">' & @CR & _
							'<name>?</name>' & @CR & _
							'</returnedTags>' & @CR & _
							'</ns:getRoutePartition>' & @CR & _
							'</soapenv:Body>' & @CR & _
							'</soapenv:Envelope>'
				Case $method = 'getLine'
						$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
						$SOAP_Action = "CUCM:DB ver=10 " & $method
						$strEnvelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.0">'&@CR& _
									   '<soapenv:Header/>'&@CR& _
									   '<soapenv:Body>'&@CR& _
										  '<ns:getLine sequence="?">'&@CR& _
											 '<pattern>'&$value100&'</pattern>'&@CR& _
											 '<routePartitionName uuid="?">'&$value101&'</routePartitionName>'&@CR& _
											 '<returnedTags uuid="?">'&@CR& _
												'<routePartitionName uuid="?">?</routePartitionName>'&@CR& _
											 '</returnedTags>'&@CR& _
										  '</ns:getLine>'&@CR& _
									   '</soapenv:Body>'&@CR& _
									'</soapenv:Envelope>'
				Case $method = 'listRoutePattern'
						$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
						$SOAP_Action = "CUCM:DB ver=10 " & $method
						$strEnvelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.0">'&@CR& _
									   '<soapenv:Header/>'&@CR& _
									   '<soapenv:Body>'&@CR& _
										  '<ns:listRoutePattern sequence="?">'&@CR& _
											 '<searchCriteria>'&@CR& _
												'<pattern>'&$value100&'</pattern>'&@CR& _
											 '</searchCriteria>'&@CR& _
											 '<returnedTags uuid="?">'&@CR& _
												'<routePartitionName uuid="?">?</routePartitionName>'&@CR& _
											 '</returnedTags>'&@CR& _
										  '</ns:listRoutePattern>'&@CR& _
									   '</soapenv:Body>'&@CR& _
									'</soapenv:Envelope>'

				Case $method = 'listCallManager'
						$postAddress = "https://" & $sHost & "/axl/ HTTP/1.1"
						$SOAP_Action = "CUCM:DB ver=10 " & $method
						$strEnvelope = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cisco.com/AXL/API/10.5">'&@CR& _
									   '<soapenv:Header/>'&@CR& _
									   '<soapenv:Body>'&@CR& _
										  '<ns:listCallManager sequence="?">'&@CR& _
											 '<searchCriteria>'&@CR& _
												'<name>'&$value100&'</name>'&@CR& _
											 '</searchCriteria>'&@CR& _
											 '<returnedTags ctiid="?" uuid="?">'&@CR& _
												'<name>?</name>'&@CR& _
											 '</returnedTags>'&@CR& _
										  '</ns:listCallManager>'&@CR& _
									   '</soapenv:Body>'&@CR& _
									'</soapenv:Envelope>'

			EndSelect
	EndSwitch


	$aXML[0] = $sVer
	$aXML[1] = $sAuth
	$aXML[2] = $sHost
	$aXML[3] = $postAddress
	$aXML[4] = $SOAP_Action
	$aXML[5] = $strEnvelope

;~ 	MsgBox(64,'Test Inside', $aXML[5])

	$value100 = ''

;~ 	_ArrayDisplay($aXML, '$aXML')
	Return $aXML

EndFunc   ;==>_XML_Formatter

Func _XML_FormatterRIS($sVersion, $sHost, $sAuth, $sMethod, $sCount, $sClass, $sStatus, $sSelectBy, $sSearch)
	Local $aXML[6], _
			$sVer, _
			$postAddress, _
			$SOAP_Action, _
			$strEnvelope, _
			$CSS_Desc, $CSS_Name

	Switch $sVersion
		Case "10"
			Select
				Case 	$sMethod = 'SelectCmDevice'
						$sVer = "10.0"
						$postAddress = "https://" & $sHost & "/realtimeservice/services/RisPort"
						$SOAP_Action = "http://schemas.cisco.com/ast/soap/action/#RisPort#" & $sMethod
						$strEnvelope = 	'<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap="http://schemas.cisco.com/ast/soap/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">' & @CR & _
											'<soapenv:Header>' & @CR & _
												'<AstHeader xsi:type="soap:AstHeader">' & @CR & _
													'<SessionId xsi:type="xsd:string">?</SessionId>' & @CR & _
												'</AstHeader>' & @CR & _
											'</soapenv:Header>' & @CR & _
											'<soapenv:Body>' & @CR & _
												'<soap:SelectCmDevice soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">' & @CR & _
													'<CmSelectionCriteria xsi:type="soap:CmSelectionCriteria">' & @CR & _
													'<MaxReturnedDevices xsi:type="xsd:unsignedInt">'&$sCount&'</MaxReturnedDevices>' & @CR & _
													'<Class xsi:type="xsd:string">'&$sClass&'</Class>' & @CR & _
													'<Status xsi:type="xsd:string">' & $sStatus & '</Status>' & @CR & _
													'<SelectBy xsi:type="xsd:string">' & $sSelectBy & '</SelectBy>' & @CR & _
													'<SelectItems xsi:type="soap:SelectItems" soapenc:arrayType="soap:SelectItem[]">' & @CR & _
														'<item xsi:type="soap:SelectItem">' & @CR & _
															'<Item xsi:type="xsd:string">' & $sSearch & '</Item>' & @CR & _
														'</item>' & @CR & _
														'</SelectItems>' & @CR & _
													'</CmSelectionCriteria>' & @CR & _
												'</soap:SelectCmDevice>' & @CR & _
											'</soapenv:Body>' & @CR & _
										'</soapenv:Envelope>'
			EndSelect
	EndSwitch


	$aXML[0] = $sVer
	$aXML[1] = $sAuth
	$aXML[2] = $sHost
	$aXML[3] = $postAddress
	$aXML[4] = $SOAP_Action
	$aXML[5] = $strEnvelope

;~ 	_ArrayDisplay($aXML, 'aXML 1')


	Return $aXML

EndFunc   ;==>_XML_FormatterRIS












