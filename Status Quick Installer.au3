#include <Services.au3>
#include <FileConstants.au3>
#include <MsgBoxConstants.au3>

#include ".\miscLibrary\misc.au3"

;~ Global	$sInstallDir ;Selected later during install process
Global $sArch = @CPUArch ;"X86" vs "X64"
Global Const $sStatusTracker = "Status Tracker", _
		$fStatusTracker = ".\Status Tracker.exe", _
		$fsqlDLLx86 = ".\sqlite3.dll", _
		$fsqlDLLx64 = ".\sqlite3_x64.dll", _
		$fStatusViewer = ".\Status Viewer.exe", _
		$fStatusReports = ".\Status Reports.exe", _
		$sServiceName = "CUCMStatTrack", _
		$sServiceDesc = "CUCM Status Tracker", _
		$fILE = ".\Logs\Install.log"

Global $bDEBUG = False, $bStdNotify = True, $bVerbose = True, $bError = True, $iInstall = 1

_Log("******************************************************" & @CRLF & "Status Tracker installation initiated" & @CRLF &, $fILE)
_Main()

Func _Main()
	Local $Region = "Region - Main", $sError, $sInstallDir, $iRtn, $bExist

	_Log($Region & ", Global, Main activated", $fILE)

	If IsAdmin() Then
		_Log($Region & ",Global, Admin rights detected. Proceeding", $fILE)
	Else
		_Log($Region & ",Global, Admin rights NOT detected. Installation must be run as administrator. " & @CRLF & "Exiting system.", $fILE, True, $MB_ICONERROR)
		_Exit(-1)
	EndIf

	$bExist = _Service_Exists($sServiceName)
	If $bExist And $iInstall = 1 Then
		If MsgBox($MB_OKCANCEL + $MB_ICONINFORMATION, "Re-install", "The service already exist. Do you want to re-install it?" & _
				@CRLF & "This will remove the service before proceeding.") = 1 Then
			_RemoveService($sServiceName)
			$bExist = 0
		Else
			If $bStdNotify Then MsgBox($MB_ICONINFORMATION, "Exit", "Exiting installation.")
			_Exit(-2)
		EndIf
	EndIf

	If $iInstall = 1 Then

		$sInstallDir = FileSelectFolder("Installation Path", @ProgramFilesDir, Default, @ProgramFilesDir) & "\CUCM Status Tracker"
		If @error Then
			$sError = "Failed to retrieve installation folder path. User cancelled folder selector." & @CRLF & "Aborting installation."
			If $bError Then _Log($Region & ", Error, " & $sError, $fILE, True, $MB_ICONERROR)
			$sError = ""
			_Exit(-3)
		Else
			If $bVerbose Then _Log($Region & ",Info, Install directory path: " & $sInstallDir, $fILE)
			DirCreate($sInstallDir)
			If @error Then
				$sError = "Failed to create installation folder. @error:" & @error & "." & @CRLF & "Aborting installation."
				If $bError Then _Log($Region & ", Error, " & $sError, $fILE, True, $MB_ICONERROR)
				$sError = ""
				_Exit(-3)
			Else
				If $bVerbose Then _Log($Region & ",Info, Successfully created installation folder: " & $sInstallDir, $fILE)
			EndIf
		EndIf


		FileInstall($fStatusTracker, $sInstallDir & "\")
		If @error Then
			$sError = "Failed to install '" & $fStatusTracker & "'. @error:" & @error & "." & @CRLF & "Aborting installation."
			If $bError Then _Log($Region & ", Error, " & $sError, $fILE, True, $MB_ICONERROR)
			$sError = ""
			_Exit(3)
		Else
			If FileExists($sInstallDir & $fStatusTracker) Then
				If $bVerbose Then _Log($Region & ",Info, Successfully installed '" & $fStatusTracker & "'.", $fILE)
			Else
				$sError = "Failed to detect '" & $fStatusTracker & "'." & @CRLF & "Aborting installation."
				If $bError Then _Log($Region & ", Error, " & $sError, $fILE, True, $MB_ICONERROR)
				$sError = ""
				_Exit(3)
			EndIf
		EndIf
		#cs
			FileInstall($fStatusViewer, $sInstallDir&"\")
			If @error Then
			$sError = "Failed to install '"&$fStatusViewer&"'. @error:"&@error&"."&@CRLF&"Aborting installation."
			If $bError Then _Log($Region &", Error, "&$sError, $fILE, True, $MB_ICONERROR)
			$sError = ""
			_Exit(4)
			Else
			If FileExists($sInstallDir&$fStatusViewer) Then
			If $bVerbose Then _Log($Region &",Info, Successfully installed '"&$fStatusViewer&"'.", $fILE)
			Else
			$sError = "Failed to detect '"&$fStatusViewer&"'."&@CRLF&"Aborting installation."
			If $bError Then _Log($Region &", Error, "&$sError, $fILE, True, $MB_ICONERROR)
			$sError = ""
			_Exit(4)
			EndIf
			EndIf

			FileInstall($fStatusReports, $sInstallDir&"\")
			If @error Then
			$sError = "Failed to install '"&$fStatusReports&"'. @error:"&@error&"."&@CRLF&"Aborting installation."
			If $bError Then _Log($Region &", Error, "&$sError, $fILE, True, $MB_ICONERROR)
			$sError = ""
			_Exit(5)
			Else
			If FileExists($sInstallDir&$fStatusReports) Then
			If $bVerbose Then _Log($Region &",Info, Successfully installed '"&$fStatusReports&"'.", $fILE)
			Else
			$sError = "Failed to detect '"&$fStatusReports&"'."&@CRLF&"Aborting installation."
			If $bError Then _Log($Region &", Error, "&$sError, $fILE, True, $MB_ICONERROR)
			$sError = ""
			_Exit(5)
			EndIf
			EndIf
		#ce
		Switch $sArch
			Case "X86"
				FileInstall($fsqlDLLx86, $sInstallDir & "\")
				If @error Then
					$sError = "Failed to install '" & $fsqlDLLx86 & "'. @error:" & @error & "." & @CRLF & "Aborting installation."
					If $bError Then _Log($Region & ", Error, " & $sError, $fILE, True, $MB_ICONERROR)
					$sError = ""
					_Exit(6)
				Else
					If FileExists($sInstallDir & $fsqlDLLx86) Then
						If $bVerbose Then _Log($Region & ",Info, Successfully installed '" & $fsqlDLLx86 & "'.", $fILE)
					Else
						$sError = "Failed to detect '" & $fsqlDLLx86 & "'." & @CRLF & "Aborting installation."
						If $bError Then _Log($Region & ", Error, " & $sError, $fILE, True, $MB_ICONERROR)
						$sError = ""
						_Exit(6)
					EndIf
				EndIf
			Case "X64"
				FileInstall($fsqlDLLx64, $sInstallDir & "\")
				If @error Then
					$sError = "Failed to install '" & $fsqlDLLx64 & "'. @error:" & @error & "." & @CRLF & "Aborting installation."
					If $bError Then _Log($Region & ", Error, " & $sError, $fILE, True, $MB_ICONERROR)
					$sError = ""
					_Exit(6)
				Else
					If FileExists($sInstallDir & $fsqlDLLx64) Then
						If $bVerbose Then _Log($Region & ",Info, Successfully installed '" & $fsqlDLLx64 & "'.", $fILE)
					Else
						$sError = "Failed to detect '" & $fsqlDLLx64 & "'." & @CRLF & "Aborting installation."
						If $bError Then _Log($Region & ", Error, " & $sError, $fILE, True, $MB_ICONERROR)
						$sError = ""
						_Exit(6)
					EndIf
				EndIf
		EndSwitch

		$iRtn = _InstallService($sInstallDir & $fStatusTracker)
	Else
		$bExist = _Service_Exists($sServiceName)
		If $bExist Then
			$iRtn = _RemoveService($sServiceName)
		Else
			_Log($Region & ",Global,The service '" & $sServiceName & "' was not found on this devices.", $fILE)
			MsgBox($MB_OKCANCEL + $MB_ICONINFORMATION, "Uninstall", "The service '" & $sServiceName & "' was not found on this devices.")
			_Exit(8)
		EndIf


	EndIf
	_Exit($iRtn)

EndFunc   ;==>_Main

Func _InstallService($sSTinstall)
	Local $Region = "Region - Install Service", $sError, $bExist, $iRtn, $error

	_Log($Region & ", Global, Install Service activated.", $fILE)

	$iRtn = _Service_Create($sServiceName, $sServiceDesc, $SERVICE_WIN32_OWN_PROCESS, $SERVICE_AUTO_START, $SERVICE_ERROR_NORMAL, $sSTinstall)
	If @error Then
		$error = @error
		$sError = "Failed to install service: " & $sServiceName & "'. @error:" & @error & "." & @CRLF & "Aborting installation."
		If $bError Then _Log($Region & ", Error, " & $sError, $fILE, True, $MB_ICONERROR)
		$sError = ""
		_Exit($error)
	Else
		$bExist = _Service_Exists($sServiceName) ; Validate that it is now installed
		If $bExist Then
			If $bVerbose Then _Log($Region & ",Info, Successfully installed service '" & $sServiceName & "'.", $fILE)
			Return $iRtn
		Else
			$sError = "Failed to detect service: " & $sServiceName & "' after installation." & @CRLF & "Aborting installation."
			If $bError Then _Log($Region & ", Error, " & $sError, $fILE, True, $MB_ICONERROR)
			$sError = ""
			_Exit(7)
		EndIf


	EndIf

EndFunc   ;==>_InstallService

Func _RemoveService($sServiceName)
	Local $Region = "Region - Remove Service", $sError, $iRtn

	$iRtn = _Service_Delete($sServiceName)
	If @error Then
		$error = @error
		$sError = "Failed to remove existing service: " & $sServiceName & "'. @error:" & @error & "." & @CRLF & "Aborting installation."
		If $bError Then _Log($Region & ", Error, " & $sError, $fILE, True, $MB_ICONERROR)
		$sError = ""
		_Exit($error)
	Else
		If $bVerbose Then _Log($Region & ",Info, Successfully removed service '" & $sServiceName & "'.", $fILE)
		Return $iRtn
	EndIf


EndFunc   ;==>_RemoveService

Func _Exit($iExitCode)
	Local $Region = "Region - System _Exit"

	_Log($Region & ", Global, Exiting System.", $fILE)
	Switch $iExitCode
		Case 1
			If $iInstall = 1 Then
				$iExitCode &= " - Successful installation of Status Tracker services."
			Else
				$iExitCode &= " - Uninstall of Status Tracker services was successful."
			EndIf

		Case 2
			$iExitCode &= " - Directory installation creation error."

		Case 3
			$iExitCode &= " - Status Tracker executable installation error."

		Case 4
			$iExitCode &= " - Status Viewer executable installation error."

		Case 5
			$iExitCode &= " - Status Reports executable installation error."

		Case 6
			$iExitCode &= " - SQLite DLL installation error."

		Case 7
			$iExitCode &= " - Failed to detect service after installation."

		Case -1
			$iExitCode &= "User does not have necessary access. Administrative rights are required."

		Case -2
			$iExitCode &= " - User terminated with previous installation found."

		Case -3
			$iExitCode &= " - User terminated by canceling folder selector."

		Case Else
			$iExitCode &= " - Installation terminated due to unknown return code. Likely service related error."

	EndSwitch
	_Log($Region & ", Global, Exiting Code: " & $iExitCode, $fILE)
	Exit $iExitCode
EndFunc   ;==>_Exit

