#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Version=Beta
#AutoIt3Wrapper_Icon=images\tracker.ico
#AutoIt3Wrapper_Outfile=Status Tracker_86.exe
#AutoIt3Wrapper_Outfile_x64=Status Tracker_64.exe
#AutoIt3Wrapper_Compile_Both=y
#AutoIt3Wrapper_Res_Comment=Status Tracker is designed to track the status of Cisco Unified Communications Manager phones - Icon made by Freepik from www.flaticon.com
#AutoIt3Wrapper_Res_Description=Status Tracker is designed to track the status of Cisco Unified Communications Manager phones
#AutoIt3Wrapper_Res_Fileversion=0.3.0.0
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#include <date.au3>
#include <File.au3>
#include <Array.au3>
#include <Debug.au3>
#include <String.au3>
#include <SQLite.au3>
#include <SQLite.dll.au3>

#include ".\xmlBOX\XML.au3"
#include ".\soapBOX\soap_exec.au3"
#include ".\soapBOX\soap_form.au3"

#Region - To Do
#cs

-Build double check that modtracker updates where successful - Done
-Change from 0 & 1 in modtracker to Reg, Rej, UnR, Par,Unk - Done
-Make this line functional: ElseIf $dModTracker = $sG_NoExist Then - Done
-Create treatment for pausing of the service/app. If the DB was open test that it doesn't error it out. If it's waiting on a response from CUCM ensure it doesn't error out.
-Create backup of DB in between transactions. Keep track of the last once committed somewhere in the event of an unexpected termination. Also create instance backups.
-Create instance map to keep track of each instance run and status of devices
-$iRententionCount = 30, $iRententionType = 1, ; Figure out if going to use or not

#ce
#EndRegion
;Icon made by Freepik from www.flaticon.com

Opt("MustDeclareVars", 1)
; This COM Error Hanlder will be used globally (excepting inside UDF Functions)
Global $oErrorHandler = ObjEvent("AutoIt.Error", ErrFunc_CustomUserHandler_MAIN)
#forceref $oErrorHandler

#Region - Variable Declaration
Global $bSync = False ; This variable is used to determine if the system is synchronization with CUCM. Will turn true during automatic sync and manual.
Global $hDB, $bDBexist = True, $iTotalListCount, $skip = False, _
		$dRunTime

Global Const 	$sG_TInstance = "instances", $sG_TBuilt = "built", $sG_TMods = "mods", $sG_TBatch = "batch", $sG_TStatus = "status", $sG_TDevices = "devices", _
				$sG_Instance = "Instance", $sG_DName = "DName", $sG_Found = "Found", $sG_ModTracker = "ModTracker", $sG_Batch = "Batch", $sG_Status = "Status", _
				$sG_LKnownIP = "LKnownIP", $sG_AddDate = "AddDate" , $sG_LKnownRegDate= "LKnownRegDate", $sG_LknownRegStat = "LknownRegStat", $sG_CRegStat = "CRegStat", _
				$sG_EpochStamp="Epoch", $sG_Registered = "Registered", $sG_UnRegistered = "UnRegistered", $sG_PartiallyReg = "PartiallyRegistered", $sG_Rejected = "Rejected", $sG_Unknown = "Unknown", _
				$aTables[6][2] = 	[[$sG_TInstance,$sG_Instance], _ ; Set at _Startup
										[$sG_TBuilt,$sG_DName&","&$sG_Found], _ ; Set at _ListPhone_Processor   w/ BnB
										[$sG_TMods, $sG_DName], _ ; Set at _SQLaddDev
										[$sG_TBatch, $sG_Batch], _
										[$sG_TStatus, $sG_Status], _
										[$sG_TDevices, $sG_DName&","&$sG_LKnownIP&","&$sG_AddDate&","&$sG_LKnownRegDate&","&$sG_LknownRegStat&","&$sG_CRegStat&","&$sG_EpochStamp&"," & _ ;$sG_EpochStamp is the Epoch time from CUCM.
													$sG_Instance]], _
				$iInstance = 0, $iBuilt = 1, $iMod = 2, $iBatch = 3, $iStatus = 4, $iDevices = 5, $iName = 0, $iColumns = 1, _
				$sG_NoExist = "No Existance", _
				$sDevStatus = "Any", _
				$sClass = "Phone", _
				$sSelectDevBy = "Name", _
				$dNow = _NowCalc()

;Debugging
;$SHOW1 & $SHOW2 will place the SOAP send and SOAP receive respectively to the console
$bSHOW1 = True
$bSHOW2 = False
Global $bDEBUG = False, $bVerbose = False, $bError = True

;Settings
Global $sVersion
#EndRegion - Variable Declaration

; This is SetUp for the transfer UDF internal COM Error Handler to the user function
_XML_ComErrorHandler_UserFunction(ErrFunc_CustomUserHandler_XML)
If FileExists("C:\Users\"&@UserName&"\Dropbox\Code\secure\st_settings.ini") Then
	Global $fINI = "C:\Users\"&@UserName&"\Dropbox\Code\secure\st_settings.ini"
	ConsoleWrite("Found ini @ C:\Users\"&@UserName&"\Dropbox\Code\secure\st_settings.ini"&@CRLF)
Else
	Local $fINI = ".\st_settings.ini"
	ConsoleWrite("Found ini @ .\st_settings.ini"&@CRLF)
EndIf

;Load Settings ini file
Local $Region = "Region - Pre-Load"

;Global INI settings

Global 	$sLogDir = IniRead($fINI, "General", "LogDir", "invalid"), _
		$sDB = IniRead($fINI, "General", "DB", "invalid"), _
		$sFilerDir = IniRead($fINI, "General", "FilerDir", "invalid"), _
		$sCUCM_Ver = IniRead($fINI, "General", "CUCM_Ver", "invalid"), _
		$sHostName_IP_Port = IniRead($fINI, "General", "HostName_IP_Port", "invalid"), _
		$sAuthorization = IniRead($fINI, "General", "Authorization", "invalid"), _
		$iDevCount = Int(IniRead($fINI, "General", "DevCount", "invalid")), _
		$iStoreInstances = Int(IniRead($fINI, "General", "StoreInstances", "invalid")), _
		$iRententionCount = Int(IniRead($fINI, "General", "RententionCount", "invalid")), _
		$iRententionType = Int(IniRead($fINI, "General", "RententionType", "invalid")), _ ;1 = Instances; 2 = Days; 3 = Both (which ever come first
		$bNewLog = IniRead($fINI, "General", "NewLog", "invalid"), _
		$bError = IniRead($fINI, "General", "LogErrors", "invalid")


If Not FileExists($sLogDir) Then
	$sLogDir = '.\Logs'
	_Log($Region & ", Global,Unable to validate log path. Setting manual path at './Logs'.")
EndIf

If Not FileExists($sFilerDir) Then
	$sFilerDir = '.\filer'
	_Log($Region & ", Global,Unable to validate FileDir path. Setting manual path at '.\filer'.")
EndIf

If Not $sCUCM_Ver = '10' Then
	$sCUCM_Ver = '10'
	_Log($Region & ", Global,Unable to validate CUCM_Ver. Setting manual path at '10'.")
EndIf

If Not Ping(StringReplace($sHostName_IP_Port,":8443","")) Then
	_Log($Region & ", Global,Unable to ping IP:"&$sHostName_IP_Port&". System cannot continue. Exiting program.")
	_Shutdown()
EndIf

Local $aXML = _XML_Formatter($sCUCM_Ver, $sHostName_IP_Port, $sAuthorization, "listCallManager", "%")
Local $sSOAP = _SOAP_Open($aXML[0], $aXML[1], $aXML[2], $aXML[3], $aXML[4], $aXML[5])
If StringInStr($sSOAP, "Error report") Then
	_Log($Region & ", Global,Unable to access CUCM with given parameters (IP & Creds). Please validate IP & credentials are correct. Exiting program.")
	_Shutdown()
EndIf

If $iDevCount > 900 Then
	$iDevCount = 900
	_Log($Region &", Global, DevCount set to 900. Maximum allowed DevCount is 900 devices. To exceed this greatly increases the odds that the max allowed device returned "& _
					"is hit and returns are lost. Lowering increases runtime.")
ElseIf $iDevCount < 1 Then
	$iDevCount = 900
	_Log($Region &", Global, DevCount set to 900. Minimum allowed DevCount is 1 devices. To exceed this greatly increases the odds that the max allowed devices returned "& _
					"is hit and returns are lost. Raising decreases runtime.")
ElseIf Not IsInt($iDevCount) Then
	$iDevCount = 900
	_Log($Region &", Global, DevCount must be of type integer. Setting to 900.")
EndIf

If $iStoreInstances < 1 Then
	$iStoreInstances = 90
	_Log($Region &", Global, StoreInstances set to default 90 days. Minimum allowed Instances stored is 1.")
EndIf

If $iRententionCount < 1 Then
	$iRententionCount = 30
	_Log($Region &", Global, Retention count set to default 30. Minimum allowed Instances stored is 1.")
EndIf

If $iRententionType < 1 or $iRententionType > 3 Then
	$iRententionType = 1
	_Log($Region &", Global, Retention type set to default, 1. Minimum allowed Instances stored is 1. 1 = Instances; 2 = Days; 3 = Both (which ever come first")
EndIf

If $bNewLog <> True And $bNewLog <> False Then
	$bNewLog = True
	_Log($Region &", Global, NewLog must be of boolean type. Set to default True.")
EndIf

If $bError <> True And $bError <> False Then
	$bError = True
	_Log($Region &", Global, LogErrors must be of boolean type. Set to default True.")
EndIf

Global Const $aINI[12] = [$sLogDir, $sDB, $sFilerDir, $sCUCM_Ver, $sHostName_IP_Port, $sAuthorization, $iDevCount, $iStoreInstances, $iRententionCount, $iRententionType, $bNewLog, $bError]

_Log("******************************************************"&@CRLF&"Status Tracker initiated."&@CRLF)

_Main()

#Region - Main Function
; #FUNCTION# ====================================================================================================================
; Name ..........: _Main
; Description ...:
; Syntax ........: _Main()
; Parameters ....:
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _Main()
	Local $Region = "Region - Main"

	_Initiate_Contact()
	exit
	_Initiate_Gather()
;~ 	_Test(5)

EndFunc   ;==>_Main
#EndRegion - Main Function

#Region - Contact

; #FUNCTION# ====================================================================================================================
; Name ..........: _Initiate_Contact
; Description ...:
; Syntax ........: _Initiate_Contact()
; Parameters ....:
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _Initiate_Contact()
	Local $Region = "Region - Iniate Contact", $iLogRetention, $aLogs

	If StringRight($sLogDir, 1) = "\" Then
		$sLogFile = $sLogDir&"LogFile.log"
		$sErrorLog = $sLogDir&"ErrorFile.log"
	Else
		$sLogFile = $sLogDir&"\LogFile.log"
		$sErrorLog = $sLogDir&"\ErrorFile.log"
	EndIf

	If $bNewLog = "True" Then
		FileDelete($sLogFile)
	Else
		$sLogFile = $sLogDir&"\LogFile_" & @YEAR & "." & @MON & "." & @MDAY & " - " & @HOUR & "." & @MIN & ".log"

		$aLogs = _FileListToArray($sLogDir, "LogFile_*.log", $FLTA_FILES, Default)
		If @error = 4 Then
			;no files found. Probably 1st run
		Else
			;~ _ArrayDisplay($aLogs)
			If $aLogs[0] > 0 Then
				_ArrayDelete($aLogs,0)
				DirCreate($sLogDir&"\archives\")
				For $entry in $aLogs
					FileMove($sLogDir&"\"&$entry, $sLogDir&"\archives\", 8)
					ConsoleWrite($Region & ",Global, Moving :"&$sLogDir&"\"&$entry&@CRLF)
				Next
			EndIf
		EndIf
	EndIf

	_Log("******************************************************Status Tracker initiated******************************************************"&@CRLF)
	_Log($Region & ",Global, Initiate Contact activated")
	_Log($Region & ",Global, Completed reading of ini settings file.")
	_Log($Region & ",Global, LogDir: "&$sLogDir)
	_Log($Region & ",Global, DB: "&$sDB)
	_Log($Region & ",Global, FilerDir: "&$sFilerDir)
	_Log($Region & ",Global, CUCM_Ver: "&$sCUCM_Ver)
	_Log($Region & ",Global, HostName_IP_Port: "&$sHostName_IP_Port)
	_Log($Region & ",Global, Authorization: **********")
	_Log($Region & ",Global, DevCount: "&$iDevCount)
	_Log($Region & ",Global, StoreInstances: "&$iStoreInstances)
	_Log($Region & ",Global, RententionCount: "&$iRententionCount)
	_Log($Region & ",Global, RententionType: "&$iRententionType)
	_Log($Region & ",Global, NewLog: "&$bNewLog)
	_Log($Region & ",Global, LogFile: "&$sLogFile)
	_Log($Region & ",Global, ErrorFile: "&$sErrorLog)

	For	$entry in $aINI
		If $entry = "invalid" Then
			_Log($Region & ",Error, Unable to read all ini Settings. Shutting down system.")
			_Shutdown(-1)
		EndIf
	Next

	If Not Ping(StringTrimRight($sHostName_IP_Port, 5)) Then
		If $bError Then _Log($Region & ", Error, Unable to ping CUCM. Terminating program.")
		Exit
	Else
		_Log($Region & ", Global, Successful ping of CUCM.")
	EndIf

	Switch @CPUArch
		Case "X86"
			If Not FileExists(".\sqlite3.dll") Then
				If $bError Then _Log($Region & ", Error, 'sqlite3.dll' detection failed. Terminating program.")
				Exit
			Else
				_Log($Region & ", Global, 'sqlite3.dll' detected.")
			EndIf

		Case "X64"
			If Not FileExists(".\sqlite3_x64.dll") Then
				If $bError Then _Log($Region & ", Error, 'sqlite3_x64.dll' detection failed. Terminating program.")
				Exit
			Else
				_Log($Region & ", Global, 'sqlite3_x64.dll' detected.")
			EndIf
	EndSwitch

EndFunc   ;==>_Initiate_Contact

#EndRegion - Contact

#Region - Gather

; #FUNCTION# ====================================================================================================================
; Name ..........: _Initiate_Gather
; Description ...:
; Syntax ........: _Initiate_Gather()
; Parameters ....:
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _Initiate_Gather()
	Local $Region = "Region - Iniate Gather", $aPhones, $aBatches, $sRtnProcessor, $sTemp, $sResults, $aResults, $iRows, $iCols, $sReturn, $sPrevInstance

	_Log($Region & ", Global,Initiate Gather activated")

	If _Startup() = "Error" Then
		_Log($Region & ",Error, Startup failed. Terminating program.")
		_Shutdown(-1)
	EndIf

	If _ListPhone_Processor() = "Error" Then
		_Log($Region & ",Error, Listphone failed. Terminating program.")
		_Shutdown(-1)
	EndIf

	If _Batch($aTables[$iDevices][$iName], 'DName') = 'Error' Then
		If $bError Then _Log($Region & ", Error, Batching failed. Terminating program.")
		_Shutdown(-1)
	EndIf


	If _SelectCMDevices() = "Error" Then
		_Log($Region & ", Error,SelectCMDevices failed. Terminating program.")
		_Shutdown(-1)
	EndIf

	If _Unknown_Processor($aTables[$iDevices][$iName]) = "Error" Then
		_Log($Region & ", Error,Unknown Processor failed. Terminating program.")
		_Shutdown(-1)
	EndIf

	$sResults = _SQLite_GetTable2d($hDB, "SELECT * FROM devices;", $aResults, $iRows, $iCols)
	If $sReturn = $SQLITE_OK Then
		_Log($Region & ", Global, Rows: " & $iRows  &  "	-	Columns: " & $iCols & @CRLF)
		_FileWriteFromArray($sFilerDir & "\Status_Export_lastRun.csv", $aResults, Default, Default, ",")
	Else
		_Log($Region & ", Error, SQLite Error: " & _SQLite_ErrMsg())
		_Log($Region & ", Error, Export Devices to csv failed.")
	EndIf

	_Shutdown(0)

EndFunc   ;==>_Initiate_Gather

#EndRegion - Gather

#Region - SQL
;Activate DB
; #FUNCTION# ====================================================================================================================
; Name ..........: _Startup
; Description ...:
; Syntax ........: _Startup($aTables)
; Parameters ....: $aTables             - a string value.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _Startup()
	Local $Region = "Region - SQL Startup", $fDLLsql, $sSQL_ver, $sReturn, $fDBexist, $aArray, $iR, $iC, $iMax, $aRow, $sMsg, $sPrevInstance, $sTemp;, $aInstances[$iStoreInstances], $sInstances
	_Log($Region & ",Global, SQLite Startup activated.")

	_Log($Region & ",Global, Backing up SQL DB.")
	If FileExists($sDB) Then
		If Not FileCopy($sDB, $sDB&".bak", 1) Then
			_Log($Region & ", Error, Error copying DB. Shutting down system")
			Return "Error"
		EndIf
	EndIf

	_Log($Region & ", Global, Starting SQL services.")
	$fDLLsql = _SQLite_Startup()
	If @error Then
		_Log($Region & ", Error @  _SQLite_Startup")
		_Log($Region & ", SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
		Exit -1
	EndIf
	_Log($Region & ", Global," & $fDLLsql)

	$sSQL_ver = "_SQLite_LibVersion=" & _SQLite_LibVersion()
	_Log($Region & ", Global," & $sSQL_ver)

	$fDBexist = FileExists($sDB)

	If $fDBexist Then
		_Log($Region & ", Global, DB exist.")
		$hDB = _SQLite_Open($sDB)
		If @error Then
			_Log($Region & ", Error, Error opening DB. @error: " & @error)
			_Log($Region & ", Error, SQLite Error, Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
			Return "Error"
		Else
			_Log($Region & ", Global,Successfully opened DB: " & $sDB)
		EndIf

		;check that all tables are built
		_Log($Region & ", Global, Getting tables from DB.")
		_SQLite_GetTable($hDB, "select tbl_name from sqlite_master where type = 'table' order by ""Table"";", $aArray, $iR, $iC)
		If @error And _SQLite_ErrCode() <> 0 Then
			_Log($Region &", Error, SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
			Return "Error"
		EndIf

		_ArrayDelete($aArray, '0-1')

		If UBound($aArray) < 6 Then
			_Log($Region & ", Error, Found "&UBound($aArray)&" tables in DB. DB is corrupt or not fully formed.")
			$fDBexist = False
			_Log($Region & ", Global,Closing DB.")
			_SQLite_Close()

			_Log($Region & ", Global,Shutting down SQLite.")
			_SQLite_Shutdown()

			_Log($Region & ", Global, Purging corrupt DB.")

			If Not FileDelete($sDB) Then
				_Log($Region & ", Global, Unable to purge corrupt DB. Terminating system.")
				Return "Error"
			Else
				_Log($Region & ", Global, DB successfully purged.")
				_Log($Region & ", Global, Starting SQL services.")
				$fDLLsql = _SQLite_Startup()
				If @error Then
					_Log($Region & ",Error @  _SQLite_Startup")
					_Log($Region & ",SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
					Return "Error"
				EndIf
				_Log($Region & ",Global," & $fDLLsql)

				$sSQL_ver = "_SQLite_LibVersion=" & _SQLite_LibVersion()
				_Log($Region & ",Global," & $sSQL_ver)

				$hDB = _SQLite_Open($sDB)
				If @error Then
					_Log($Region & ",Error,Error opening DB. @error: " & @error)
					_Log($Region & ",SQLite Error, Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
					Return "Error"
				Else
					_Log($Region & ",Global,Successfully opened new DB: " & $sDB)
				EndIf
				$fDBexist = False
			EndIf
		Else
			_Log($Region & ", Global, Found "&UBound($aArray)&" tables in DB. DB is intact.")
		EndIf
	Else
		_Log($Region & ", Global, DB does not exist. This is the 1st run. Creating DB:"&$sDB&".")
		$hDB = _SQLite_Open($sDB)
		If @error Then
			_Log($Region & ",Error,Error opening DB. @error: " & @error)
			_Log($Region & ",SQLite Error, Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
			Return "Error"
		Else
			_Log($Region & ",Global,Successfully opened new DB: " & $sDB)
		EndIf
	EndIf

	If Not $fDBexist Then
		_Log($Region & ",Info," & $sDB & " not found. Creating DB.")
		$bDBexist = False

		For $s = 0 To UBound($aTables) - 1
			Switch $aTables[$s][$iName]
				Case 'instances' ;"Instance"
					_Log($Region & ",Info, Creating table:" & $aTables[$s][$iName] & ".")
					$sReturn = _SQLite_Exec($hDB, "CREATE TABLE IF NOT EXISTS " & $aTables[$s][$iName] & " (id INTEGER NOT NULL PRIMARY KEY, " &  $aTables[$s][$iColumns] & ", UNIQUE("&$sG_Instance&"));")
					If @error Then
						_Log($Region & ",Error,Error writing " & $aTables[$s][$iName] & " table to DB. @error: " & @error)
						_Log($Region & ",Error, Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
						_Log($Region & ",Error, Terminating system.")
						Return "Error"
					Else
						_Log($Region & ",Info,Successfully created " & $aTables[$s][$iName] & " table: " & $sReturn)
					EndIf

#cs
					#Region - Build all instances based on $iStoreInstances
					_Log($Region & ",Info, Updating table:" & $aTables[$s][$iName] & " with full NULL instances.")
					$sReturn = _SQL_BnB_Processor($aTables[$s][$iName], $sG_Instance, $aInstances)
					#EndRegion - Build all instances based on $iStoreInstances
					If $sReturn = "Error" Then
						_Log($Region & ",Error, Error writing NULL values to table: " & $aTables[$s][$iName] & ". @error: " & @error)
						_Log($Region & ",Error, SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
						_Log($Region & ",Error, Terminating system.")
						Return "Error"
					Else
						_Log($Region & ",Info, Successfully added " & $aTables[$s][$iColumns] & " values '"&$dNow&"' to table: " & $aTables[$s][$iName])
					EndIf
#ce

					_Log($Region & ",Info, Updating table:" & $aTables[$s][$iName] & " with 1st instance:"&$dNow)
					_SQLite_Exec($hDB, "INSERT INTO " & $aTables[$s][$iName] & " (" & $aTables[$s][$iColumns] & ") VALUES ('"&$dNow&"');")
					If @error Then
						_Log($Region & ",Error,Error writing  values to table: " & $aTables[$s][$iName] & ". @error: " & @error)
						_Log($Region & ",Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
						_Log($Region & ",Error, Terminating system.")
						Return "Error"
					Else
						_Log($Region & ",Info,Successfully added " & $aTables[$s][$iColumns] & " values '"&$dNow&"' to table: " & $aTables[$s][$iName])
					EndIf

				Case 'status' ;"status"
					_Log($Region & ",Info, Creating table:" & $aTables[$s][$iName] & ".")
					$sReturn = _SQLite_Exec($hDB, "CREATE TABLE IF NOT EXISTS " & $aTables[$s][$iName] & " (id INTEGER NOT NULL PRIMARY KEY, " & $aTables[$s][$iColumns] & ", UNIQUE("&$sG_Status&"));")
					If @error Then
						_Log($Region & ",Error,Error writing " & $aTables[$s][$iName] & " table to DB. @error: " & @error)
						_Log($Region & ",Error, Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
						_Log($Region & ",Error, Terminating system.")
						Return "Error"
					Else
						_Log($Region & ",Info,Successfully created " & $aTables[$s][$iName] & " table: " & $sReturn)
					EndIf

					_Log($Region & ",Info, Inserting into table:" & $aTables[$s][$iName] & ".")
					_SQLite_Exec($hDB, "INSERT INTO " & $aTables[$s][$iName] & " (" & $aTables[$s][$iColumns] & ") VALUES ('"&$sG_Registered&"'),('"&$sG_UnRegistered&"'),('"&$sG_Rejected&"'),('"&$sG_Unknown&"'), ('"&$sG_PartiallyReg&"');")
					If @error Then
						_Log($Region & ",Error,Error writing  values to table: " & $aTables[$s][$iColumns] & ". @error: " & @error)
						_Log($Region & ",Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
						_Log($Region & ",Error, Terminating system.")
						Return "Error"
					Else
						_Log($Region & ",Info,Successfully added " & $aTables[$s][$iColumns] & " values '"&$sG_Registered&"','"&$sG_UnRegistered&"','"&$sG_Rejected&"','"&$sG_Unknown&"' to table: " & $aTables[$s][$iName])
					EndIf

				Case 'built' ;"DName,Found"
					_Log($Region & ",Info, Creating table:" & $aTables[$s][$iName] & ".")
					$sReturn = _SQLite_Exec($hDB, "CREATE TABLE IF NOT EXISTS " & $aTables[$s][$iName] & " ("&$sG_DName&" STRING NOT NULL PRIMARY KEY COLLATE NOCASE, "&$sG_Found&" BOOL DEFAULT 0);")
					If @error Then
						_Log($Region & ",Error,Error writing " & $aTables[$s][$iName] & " table to DB. @error: " & @error)
						_Log($Region & ",Error, Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
						_Log($Region & ",Error, Terminating system.")
						Return "Error"
					Else
						_Log($Region & ",Info,Successfully created " & $aTables[$s][$iName] & " table: " & $sReturn)
					EndIf

				Case 'mods'; $sG_DName&","'1','2','3','4','5', ... -> $iStoreInstances
					_Log($Region & ",Info, Creating table:" & $aTables[$s][$iName] & ".")
					$sReturn = _SQLite_Exec($hDB, "CREATE TABLE IF NOT EXISTS " & $aTables[$s][$iName] & " ("&$sG_DName&" STRING COLLATE NOCASE,'"&$dNow&"' STRING DEFAULT Unk "& _
												", PRIMARY KEY ("&$sG_DName&")"& _
												", FOREIGN KEY ("&$sG_DName&") REFERENCES "&$sG_TBuilt&"("&$sG_DName&") ON DELETE CASCADE ON UPDATE CASCADE);")
												;", FOREIGN KEY ('"&$dNow&"') REFERENCES "&$sG_TInstance&"('"&$dNow&"') ON DELETE CASCADE ON UPDATE CASCADE);") ; Not true anymore
					If @error Then
						_Log($Region & ",Error,Error writing " & $aTables[$s][$iName] & " table to DB. @error: " & @error)
						_Log($Region & ",Error, Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
						_Log($Region & ",Error, Terminating system.")
						Return "Error"
					Else
						_Log($Region & ",Info,Successfully created " & $aTables[$s][$iName] & " table: " & $sReturn)
					EndIf

				Case 'devices'; $sG_DName&","&$sG_LKnownIP&","&$sG_AddDate&","&$sG_LKnownRegDate&","&$sG_LknownRegStat&","&$sG_CRegStat&","&$sG_EpochStamp&","&$sG_ModTracker&","$sG_Instance
					_Log($Region & ",Info, Creating table:" & $aTables[$s][$iName] & ".")
					$sTemp = 	$sG_DName&" COLLATE NOCASE,"&$sG_LKnownIP&" DEFAULT 'Unknown',"&$sG_AddDate&" DEFAULT 'Unknown',"&$sG_LKnownRegDate&" DEFAULT 'Unknown',"&$sG_LknownRegStat&" DEFAULT 'Unknown',"& _
								$sG_CRegStat&" DEFAULT 'Unknown',"&$sG_EpochStamp&" DEFAULT 'Unknown'," & $sG_Instance
					$sReturn = _SQLite_Exec($hDB, "CREATE TABLE IF NOT EXISTS " & $aTables[$s][$iName] & " (" & $sTemp& _
												", PRIMARY KEY ("&$sG_DName&")" & _
												", FOREIGN KEY ("&$sG_DName&") REFERENCES "&$sG_TBuilt&"("&$sG_DName&")" & _
												", FOREIGN KEY ("&$sG_Instance&") REFERENCES "&$sG_TInstance&"("&$sG_Instance&") ON DELETE CASCADE ON UPDATE CASCADE);")
												;", FOREIGN KEY ("&$sG_ModTracker&") REFERENCES "&$sG_TMods&"("&$sG_ModTracker&")" & _ ;This is no longer true
					If @error Then
						_Log($Region & ",Error,Error writing " & $aTables[$s][$iName] & " table to DB. @error: " & @error)
						_Log($Region & ",Error, Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
						_Log($Region & ",Error, Terminating system.")
						Return "Error"
					Else
						_Log($Region & ",Info,Successfully created " & $aTables[$s][$iName] & " table: " & $sReturn)
					EndIf

				Case Else
					_Log($Region & ",Info, Creating table:" & $aTables[$s][$iName] & ".")
					$sReturn = _SQLite_Exec($hDB, "CREATE TABLE IF NOT EXISTS " & $aTables[$s][$iName] & " (id INTEGER NOT NULL PRIMARY KEY, " & $aTables[$s][$iColumns] & ");")
					If @error Then
						_Log($Region & ",Error,Error writing " & $aTables[$s][$iName] & " table to DB. @error: " & @error)
						_Log($Region & ",Error, Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
						_Log($Region & ",Error, Terminating system.")
						Return "Error"
					Else
						_Log($Region & ",Info,Successfully created " & $aTables[$s][$iName] & " table: " & $sReturn)
					EndIf
			EndSwitch
		Next
	Else
		$hDB = _SQLite_Open($sDB) ;open existing DB
		If @error Then
			_Log($Region & ",Error,Error opening DB. @error: " & @error)
			_Log($Region & ",Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
			Return "Error"
		Else
			_Log($Region & ",Info,Successfully opened existing DB: " & $sDB)
		EndIf

		$sReturn = _SQLite_Exec($hDB, "DELETE From "&$aTables[$iBatch][$iName]&";") ; clear out batch. To leave it would cause both the old and new batches to run.
		If @error Then
			_Log($Region & ",Error,Error dropping batch table. @error: " & @error)
			_Log($Region & ",Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
			Return "Error"
		Else
			_Log($Region & ",Info,Successfully dropped batch table: " & $sDB)
		EndIf

		;Get Previous instance
		_SQLite_Query($hDB, "SELECT MAX(id) FROM " &$aTables[$iInstance][$iName]&";", $sMsg) ;Get the previous instance for mod.
		While _SQLite_FetchData($sMsg, $aRow) = $SQLITE_OK
			$iMax &= $aRow[0]
		WEnd

		_SQLite_QuerySingleRow($hDB, "SELECT "&$sG_Instance&" FROM "&$aTables[$iInstance][$iName]&" WHERE id='"&$iMax&"'", $aRow)
		If @error And _SQLite_ErrCode() <> 0 Then
			If $bError Then _Log($Region & ", Error, Query Error Tab;e:" & $aTables[$iInstance][$iName] & " Object:" & $sG_Instance)
			If $bError Then _Log($Region & ", Error, SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
			Return "Error"
		EndIf

		If $aRow[0] = '' Then
			_Log($Region &'Error, Error retrieving previous instance. Setting instance to orphand.')
			$sPrevInstance = "Orphand" ;This way the program will continue to run. Need to build in a notification system. Just one more thing.
		Else
			$sPrevInstance = $aRow[0] ;Got it.
		EndIf

		_SQLite_Exec($hDB, "INSERT INTO " & $aTables[$iInstance][$iName] & " (" & $aTables[$iInstance][$iColumns] & ") VALUES "& _
							"('"&$dNow&"');")
		If @error Then
			_Log($Region & ",Error,Error writing  values to table: " & $aTables[$s][$iColumns] & ". @error: " & @error)
			_Log($Region & ",Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
			Return "Error"
		Else
			_Log($Region & ",Info,Successfully added " & $aTables[$iInstance][$iColumns] & " values '"&$dNow&"' to table: " & $aTables[$iInstance][$iName])
		EndIf

		$sReturn = _SQLite_Exec($hDB, "UPDATE " & $aTables[$iBuilt][$iName] & " SET "&$sG_Found&"='0';")
		If @error Then
			If $bError Then _Log($Region & ", Error, SQLite Execution failed to enter:" & $sG_Found & " in table: " & $aTables[$iBuilt][$iName] & ". Returned: " & $sReturn)
			If $bError Then _Log($Region & ", Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
			Return "Error"
		EndIf

		_SQLite_Exec($hDB, "ALTER TABLE " & $aTables[$iMod][$iName] & " ADD '" &$dNow & "' STRING DEFAULT Unk;")
		If @error Then
			_Log($Region & ", Error, Error writing  values to table: " & $aTables[$iMod][$iName] & ". @error: " & @error)
			_Log($Region & ",Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
			Return "Error"
		Else
			_Log($Region & ",Info,Successfully added " & $aTables[$iMod][$iName] & " values '"&$dNow&"' to table: " & $aTables[$iMod][$iName])
		EndIf
#cs Redundant given I do this later anyways in BnB
		$sReturn = _SQLite_Exec($hDB, "UPDATE " & $aTables[$iDevices][$iName] & " SET "&$sG_ModTracker&"=U';")
		If @error Then
			If $bError Then _Log($Region & ", Error, SQLite Execution failed to enter:" & $sG_ModTracker & " in table: " & $aTables[$iDevices][$iName] & ". Returned: " & $sReturn)
			If $bError Then _Log($Region & ", Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
			Return "Error"
		EndIf
#ce
	EndIf

	Return "Good"

EndFunc   ;==>_Startup

;Add Entry to DB
; #FUNCTION# ====================================================================================================================
; Name ..........: _SQLaddBnB
; Description ...:
; Syntax ........: _SQLaddBnB($sTable, $sIndex, $sEntry)
; Parameters ....: $sTable              - a string value.
;                  $sIndex              - a string value.
;                  $sEntry              - a string value.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _SQLaddBnB($sTable, $sIndex, $sEntry)
	Local $Region = "Region - SQL BnB", $sReturn, $sTemp
	;New device... addDate is today, Lastknown reg status is current stat, if currently Registered then Last known reg date is today else it's unknown
	#cs
	If UBound($sEntry,2) > 1 Then
		For $entry In $sEntry
			$sTemp &= $entry&","
		Next
		$sEntry = StringTrimRight($sTemp, 1)
	EndIf
	#ce
	Select
		Case $sTable = $sG_TBuilt ;Table="built" Index="DName,Found"
			If $bVerbose Then _Log($Region &",Info,Case 1 - Matched Table: "&$sTable&" for Index: "&$sIndex&". Writing entry:"&$sEntry&".")
			$sReturn = _SQLquery($sTable, $sG_DName, $sEntry)
			If $sReturn = $sG_NoExist Then

				$sReturn = _SQLite_Exec($hDB, "INSERT INTO " & $sTable & " (" & $sIndex & ") VALUES ('"&$sEntry&"','1');") ;Note that Entry was modified to also update Found Index to 1
				If @error Then
					If $bError Then _Log($Region & ", Error, SQLite Execution failed to enter:" & $sEntry & " in table: " & $sTable & ". Returned: " & $sReturn)
					If $bError Then _Log($Region & ", Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
					Return "Error"
				Else
					If $bVerbose Then _Log($Region &",Info, INSERT "&$sIndex&" VALUES "&$sEntry&", '1'.")
					Return "Good"
				EndIf
			Else
				$sReturn = _SQLite_Exec($hDB, "UPDATE " & $sTable & " SET "&$sG_Found&"='1' WHERE "&$sG_DName&"='"&$sEntry&"';")
				If @error Then
					If $bError Then _Log($Region & ", Error,  SQLite Execution failed for device:" & $sEntry & ". Returned: " & $sReturn)
					If $bError Then _Log($Region & ", Error, Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
					Return "Error"
				Else
					If $bVerbose Then _Log($Region &",Info, UPDATE & Set "&$sIndex&" ='"&$sEntry&", '1'.")
					Return "Good"
				EndIf

			EndIf

		Case $sTable = $sG_TDevices AND $sIndex = $sG_ModTracker
			If $bVerbose Then _Log($Region &",Info,Case 2 - Matched Table: "&$sTable&" for Index: "&$sIndex&". Writing entry:"&$sEntry&".")
			$sReturn = _SQLite_Exec($hDB, "UPDATE " & $sTable & " SET "&$sIndex&"='"&$sEntry&"';")
			If @error Then
				If $bError Then _Log($Region & ", Error, SQLite Execution failed to enter:" & $sG_ModTracker & " in table: " & $sTable & ". Returned: " & $sReturn)
				If $bError Then _Log($Region & ", Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
				Return "Error"
			Else
				If $bVerbose Then _Log($Region &",Info, UPDATE & Set "&$sIndex&" ='"&$sEntry&"'.")
				Return "Good"
			EndIf

		Case $sTable = $sG_TDevices
			If $bVerbose Then _Log($Region &",Info,Case 3 - Matched Table: "&$sTable&" for Index: "&$sG_DName&","&$sG_AddDate&". Writing entry:'"&$sEntry& "','"&$dNow&"'.")
			$sReturn = _SQLite_Exec($hDB, "INSERT OR IGNORE INTO " & $sTable & " ("&$sG_DName&","&$sG_AddDate&") "& _
																	"VALUES ('"&$sEntry& "','"&$dNow&"');")
			If @error Then
				If $bError Then _Log($Region & ", Error, SQLite Execution failed to enter:" & $sEntry & " in table: " & $sTable & ". Returned: " & $sReturn)
				If $bError Then _Log($Region & ", Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
				Return "Error"
			Else
				If $bVerbose Then _Log($Region &",Info, INSERT OR IGNORE INTO " & $sTable & " ("&$sG_DName&","&$sG_AddDate&") "& "VALUES ('"&$sEntry& "','"&$dNow&"')")
				Return "Good"
			EndIf
#cs
		Case $sTable = $sG_TMods AND $sIndex = $dNow ;Is this still being used?
			If $bVerbose Then _Log($Region &",Info,Case 4 - Matched Table: "&$sTable&" for Index: "&$dNow&". Writing entry:"&$sEntry&".")
			$sReturn = _SQLite_Exec($hDB, "UPDATE " & $sTable & " SET "&$dNow&"="&$sEntry& ","&$dNow&" ;")
			If @error Then
				If $bError Then _Log($Region & ", Error,  SQLite Execution failed for device:" & $sEntry & ". Returned: " & $sReturn)
				If $bError Then _Log($Region & ", Error, Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
				Return "Error"
			Else
				If $bVerbose Then _Log($Region &",Info, UPDATE & Set "&$sIndex&" ='"&$sEntry&","&$dNow&".")
				Return "Good"
			EndIf
#ce
		Case $sTable = $sG_TMods ;Table="mod" Index="DName,date,date,$dNow"
			If $bVerbose Then _Log($Region &",Info,Case 5 - Matched Table: "&$sTable&" for Index: "&$sIndex&". Writing entry:"&$sEntry&".")
			$sReturn = _SQLquery($sTable, $sG_DName, $sEntry)
			If $sReturn = $sG_NoExist Then
				$sReturn = _SQLite_Exec($hDB, "INSERT OR IGNORE INTO " & $sTable & " (" & $sIndex & ") VALUES ('"&$sEntry&"');") ;Note that Entry was modified to also update Found Index to 1
				If @error Then
					If $bError Then _Log($Region & ", Error, SQLite Execution failed to enter:" & $sEntry & " in table: " & $sTable & ". Returned: " & $sReturn)
					If $bError Then _Log($Region & ", Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
					Return "Error"
				Else
					$sReturn = _SQLquery($sTable, $sG_DName, $sEntry)
					If $sReturn = $sG_NoExist Then
						If $bError Then _Log($Region & ", Error, SQLite Execution failed to enter:" & $sEntry & " in table: " & $sTable & ". Returned: " & $sReturn)
						If $bError Then _Log($Region & ", Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
						Return "Error"
					Else
						If $bVerbose Then _Log($Region &",Info, INSERT "&$sIndex&" VALUES "&$sEntry&".")
					EndIf
					Return "Good"
				EndIf
			Else
				Return "Exist"
			EndIf

		Case Else
			If $bVerbose Then _Log($Region &",Info,Case 6 - Matched Table: "&$sTable&" for Index: "&$sIndex&". Writing entry:"&$sEntry&".")

			If $sEntry = "NULL" Then
				$sReturn = _SQLite_Exec($hDB, "INSERT INTO " & $sTable & " (" & $sIndex & ") VALUES (" & $sEntry & ");")
			Else
				$sReturn = _SQLite_Exec($hDB, "INSERT OR IGNORE INTO " & $sTable & " (" & $sIndex & ") VALUES ('" & $sEntry & "');")
			EndIf

			If @error Then
				If $bError Then _Log($Region & ", Error, SQLite Execution failed to enter:" & $sEntry & " in table: " & $sTable & ". Returned: " & $sReturn)
				If $bError Then _Log($Region & ", Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
				Return "Error"
			Else
				If $bVerbose Then _Log($Region &",Info, INSERT "&$sIndex&" ='"&$sEntry&"'.")
				Return "Good"
			EndIf

	EndSelect


EndFunc   ;==>_SQLaddBnB

;Add Devices to DB
; #FUNCTION# ====================================================================================================================
; Name ..........: _SQLaddDev
; Description ...:
; Syntax ........: _SQLaddDev($sTable, $sHeaders, $sDName, $sLKnownIP, $sCRegStat)
; Parameters ....: $sTable              - a string value.
;                  $sHeaders            - a string value.
;                  $sDName              - a string value.
;                  $sLKnownIP           - a string value.
;                  $sCRegStat           - a string value.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _SQLaddDev($sTable, $sDName, $sLKnownIP, $sCRegStat, $sEpoch)
	;devices = $sG_DName&","&$sG_LKnownIP&","&$sG_AddDate&","&$sG_LKnownRegDate&","&$sG_LknownRegStat&","&$sG_CRegStat&","&$sG_EpochStamp&","&$sG_ModTracker&","$sG_Instance
	Local 	$Region = "Region - SQL Add Dev", $dLKnownRegDate, $sLknownRegStat, $aDBout, $sReturn, $dModTracker, $dRecentMod, $bFactor, $sTempRegStat, $iLater, $sExistingEpoch, _
			$sExistingReg, $sSQL_Command, $sRegCode = StringLeft($sCRegStat, 3)
	If $bVerbose Then _Log($Region & ", Info, SQLADD activated.")
	If $bVerbose Then _Log($Region & ", Info, Device Name: " & $sDName)
	If $bVerbose Then _Log($Region & ", Info, Last Known IP: " & $sLKnownIP)
	If $bVerbose Then _Log($Region & ", Info, Current Registration Status: " & $sCRegStat)
	If $bVerbose Then _Log($Region & ", Info, Device Epoch: " & $sEpoch)

	;The objective here is to determine if the device has been modified in the DB today.
	;If it has we need to ensure we're not overwriting a registered device with an unregistered dupe

	$dModTracker = _SQLquery($aTables[$iMod][$iName], '"'&$dNow&'"', $sDName)
	If $bVerbose Then _Log($Region & ", Info, ModTracker returned: " & $dModTracker)

	If $dModTracker = "Unk" Then
		If $bVerbose Then _Log($Region & ", Info, Device: "&$sDName&" exist, but has NOT been modifed in this instance.")

		;Update the device in the mod table since it already exist, but was zeroed out.
		$sReturn = _SQLite_Exec($hDB, "UPDATE " & $aTables[$iMod][$iName] & " SET "&'"'&$dNow&'"'&"='"&$sRegCode&"' WHERE "&$sG_DName&"='" & $sDName & "';")
		If @error Then
			If $bError Then _Log($Region & ", Error,  SQLite Execution failed for device:" & $sDName & ". Returned: " & $sReturn)
			If $bError Then _Log($Region & ", Error, Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
			Return "Error"
		Else
			If $bVerbose Then _Log($Region & ",Info,Device exist. UPDATED "&'"'&$dNow&'"'&"="&$sRegCode&" WHERE "&$sG_DName&"='" & $sDName & "'")
		EndIf
	#cs	-	Let's simply with above code. Assume latest epoch is correct. If == then take reg.
		If $bVerbose Then _Log($Region & ", Info, Device: "&$sDName&" has been modified in this instance. Compariing Epoch timestamps.")

		$sExistingEpoch = _SQLquery($sTable, $sG_EpochStamp, $sDName)
		$sExistingReg = _SQLquery($sTable, $sG_CRegStat, $sDName)

		If $bVerbose Then _Log($Region & ", Info, Instance CRegStat & Epoch: "&$sCRegStat&" - "&$sEpoch&".")
		If $bVerbose Then _Log($Region & ", Info, Existing CRegStat & Epoch: "&$sExistingReg&" - "&$sExistingEpoch&".")

		If $sEpoch > $sExistingEpoch AND $sCRegStat = $sG_Registered Then
			;Continue on
		ElseIf $sEpoch > $sExistingEpoch AND $sExistingReg = $sG_Registered Then ;it's a later date. Shouldn't be possible to overwrite an existing Registered, but let's check anyways.
			_Log($Region &",Error, Device: "&$sDName&" has a higher Epoch stamp than the the existing entry while existing CRegstat = "&$sExistingReg&". Existing Epoch: "&$sExistingEpoch&" while the new "& _
									"device has Epoch: "&$sEpoch&".")
			If $sCRegStat <> $sG_Registered Then
				_Log($Region &",Error, Device: "&$sDName&" also has a CRegstat of: "&$sCRegStat&".")
			EndIf

		ElseIf $sEpoch < $sExistingEpoch AND $sCRegStat = $sG_Registered
			_Log($Region &",Error, Device: "&$sDName&" has a lower Epoch stamp than the the existing entry while instance CRegstat = "&$sCRegStat&". Existing Epoch: "&$sExistingEpoch&" while the new "& _
									"device has Epoch: "&$sEpoch&".")
			If $sExistingReg <> $sG_Registered Then
				_Log($Region &",Error, Existing Device: "&$sDName&" also has a CRegstat of: "&$sExistingReg&".")
			EndIf
		Else
			Return "Dupe" ;older dupe entry. Skip it.
		EndIf
#ce
	ElseIf $dModTracker = $sG_NoExist Then
		;This shouldn't ever be considering we've already added every device. Make this work as some have slipped through
		If $bVerbose Then _Log($Region & ", Info, Device: "&$sDName&" does not exist in "&$sG_TMods&".")

		;Add the device to the mod table and add status since it doesn't exist.

		$sReturn = _SQLite_Exec($hDB, "INSERT INTO " & $aTables[$iMod][$iName] & " (" & $sG_DName &",'"& $dNow & "') VALUES ('"&$sDName&"','"&$sRegCode&"');") ;Note that Entry was modified to also update Found Index to 1
		If @error Then
			If $bError Then _Log($Region & ", Error,  SQLite Execution failed for device:" & $sDName & ". Returned: " & $sReturn)
			If $bError Then _Log($Region & ", Error, Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
			Return "Error"
		Else
			If $bVerbose Then _Log($Region & ",Info,Device does not exist. INSERTing INTO " & $aTables[$iMod][$iName] & " (" & $sG_DName &",'"& $dNow & "') VALUES ('"&$sDName&"','"&$sRegCode&"')")
		EndIf


	ElseIf $dModTracker <> "Unk" Then
		If $bVerbose Then _Log($Region & ", Info, Device: "&$sDName&" has been modified in this instance. Comparing Epoch timestamps.")

		$sExistingEpoch = _SQLquery($sTable, $sG_EpochStamp, $sDName)
		$sExistingReg = _SQLquery($sTable, $sG_CRegStat, $sDName)
		If $bVerbose Then _Log($Region & ", Info, Instance CRegStat & Epoch: "&$sCRegStat&" - "&$sEpoch&".")
		If $bVerbose Then _Log($Region & ", Info, Existing CRegStat & Epoch: "&$sExistingReg&" - "&$sExistingEpoch&".")

		If $sEpoch = $sExistingEpoch AND $sCRegStat = $sG_Registered Then
			;Continue on
			If $bVerbose Then _Log($Region & ", Info, Both Epochs are equal and the instance status is 'Registered'. Recording dating into DB.")
		ElseIf $sEpoch > $sExistingEpoch AND $sCRegStat = $sG_Registered Then
			;Continue on
			If $bVerbose Then _Log($Region & ", Info, The instance Epoch is greater than the recorded Epoch and the instance status is 'Registered'. Recording dating into DB.")
		ElseIf $sEpoch = $sExistingEpoch AND $sCRegStat <> $sG_Registered Then
			If $bVerbose Then _Log($Region & ", Info, Both Epochs are equal but the instance status is NOT 'Registered'. Dupe, skipping to next device entry.")
			Return "Dupe" ;older dupe entry. Skip it.
		ElseIf $sEpoch < $sExistingEpoch AND $sCRegStat <> $sG_Registered Then
			If $bVerbose Then _Log($Region & ", Info, The recorded Epoch is greater then the instance Epoch and the instance status is NOT 'Registered'. Dupe, skipping to next device entry.")
			Return "Dupe" ;older dupe entry. Skip it.
		EndIf

	Else
		_Log($Region & ",Error, Device: "&$sDName&" has encountered an error during query of "&$sG_ModTracker&". Returned: '"&$dModTracker&".")
		Return "Error"
	EndIf

	#Region - Update exsiting Device

	Select
		;Existing device... don't touch add date, lastknown reg stats is today if Reg'd else pull old current
		Case $sCRegStat = $sG_Registered Or $sCRegStat = $sG_PartiallyReg
			If $bVerbose Then _Log($Region & ", Info, Case: "&$sG_Registered&" or "&$sG_PartiallyReg&".")
			;Last known registered date should always be equal to the last time the device was detected. In this case... today
			$dLKnownRegDate = StringTrimRight($dNow, 2)

		Case $sCRegStat = $sG_UnRegistered
			If $bVerbose Then _Log($Region & ", Info, Case: "&$sG_UnRegistered&".")
			;Last known registered date should always be equal to the last time the device was detected.
			$dLKnownRegDate = _SQLquery($sTable, "LKnownRegDate", $sDName)
			If $dLKnownRegDate = $sG_NoExist Then $dLKnownRegDate = $sG_Unknown

		Case $sCRegStat = $sG_Rejected
			If $bVerbose Then _Log($Region & ", Info, Case: "&$sG_Rejected&".")
			;Last known registered date should always be equal to the last time the device was detected.
			$dLKnownRegDate = _SQLquery($sTable, "LKnownRegDate", $sDName)
			If $dLKnownRegDate = $sG_NoExist Then $dLKnownRegDate = $sG_Unknown

		Case Else
			If $bVerbose Then _Log($Region & ", Info, Case: Else.")
			$sCRegStat &= "-Anomaly"
			_Log($Region & ", Global, Anomaly detected with device: " & $sDName)
			$dLKnownRegDate = _SQLquery($sTable, "LKnownRegDate", $sDName)
			If $dLKnownRegDate = $sG_NoExist Then $dLKnownRegDate = $sG_Unknown

	EndSelect

	;Last known regstat should be equal to what's current in the DB now.
	$sLknownRegStat = _SQLquery($sTable, "CRegStat", $sDName)
	If $sLknownRegStat = $sG_NoExist Then $sLknownRegStat = $sG_Unknown

	If $bVerbose Then _Log($Region & ", Info, Current Registration Status: " & $sCRegStat)
	If $bVerbose Then _Log($Region & ", Info, Last Known Registration Date: " & $dLKnownRegDate)
	If $bVerbose Then _Log($Region & ", Info, Last Known Registration Status: " & $sLknownRegStat)


	#EndRegion - Update exsiting Device
	$sSQL_Command = "UPDATE " & $sTable & " SET "&$sG_LKnownIP&"='" & $sLKnownIP & "',"&$sG_LKnownRegDate&"='" & $dLKnownRegDate & "',"&$sG_LknownRegStat&"='" & $sLknownRegStat & _
			"',"&$sG_CRegStat&"='" & $sCRegStat & "',"&$sG_EpochStamp&"='"&$sEpoch&"',"&$sG_Instance&"="& _
			"(SELECT "&$sG_Instance&" FROM "&$sG_TInstance&" WHERE ID=(SELECT MAX(ID) FROM "&$sG_TInstance&")) WHERE "&$sG_DName&"='" & $sDName & "';"

	;Pulled modtracker out. Need to remove this one from the table all together = ',"&$sG_ModTracker&"=(SELECT "&'"'&$dNow&'"'&" FROM "&$sG_TMods&" WHERE "&$sG_DName&"='"&$sDName&"')

	;Device is pre-existing must do UPDATE
	$sReturn = _SQLite_Exec($hDB, $sSQL_Command )
	If @error Then
		If $bError Then _Log($Region & ", Error,  SQLite Execution failed for device:" & $sDName & ". Returned: " & $sReturn)
		If $bError Then _Log($Region & ", Error, Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
		Return "Error"
	Else
		If $bVerbose Then _Log($Region & ",Info,"&$sSQL_Command)
	EndIf
#cs
	Elseif $dModTracker = $sG_NoExist Then
		;Device was not found in mod table. This is a new entry, but all are already added to device... so why is this here... commmenting out.
		$sReturn = _SQLite_Exec($hDB, "INSERT OR IGNORE INTO " & $sTable & " ("&$sG_DName&","&$sG_LKnownIP&","&$sG_AddDate&","&$sG_LKnownRegDate&","&$sG_LknownRegStat&","&$sG_CRegStat&","&$sG_EpochStamp&","&$sG_Instance&") "& _
																	"VALUES ('"&$sDName& "','"&$sLKnownIP& "','"&$dNow&"','"&$dLKnownRegDate&"','" &$sLknownRegStat&"','"&$sCRegStat&"','"&$dNow&"','"&$dNow&"');")
		If @error Then
			If $bError Then _Log($Region & ", Error, SQLite Execution failed to enter:" & " ('"&$sG_DName&"','"&$sG_LKnownIP&"','"&$sG_AddDate&"','"&$sG_LKnownRegDate&"','"&$sG_LknownRegStat&"','"& _
																							$sG_CRegStat&"','"&$sG_EpochStamp&"','"&$sG_Instance&"') " & " in table: " & $sTable & ". Returned: " & $sReturn)
			If $bError Then _Log($Region & ", Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
			Return "Error"
		EndIf
	Else
		If $bError Then _Log($Region &", Error, An unidentified modTracker was identified for: "&$sDName&". No Insert or Update was made for this device.")
	EndIf
#ce

	Return $sReturn
EndFunc   ;==>_SQLaddDev

;Search DB
; #FUNCTION# ====================================================================================================================
; Name ..........: _SQLquery
; Description ...:
; Syntax ........: _SQLquery($sTable, $iNdex, $sObject)
; Parameters ....: $sTable              - a string value.
;                  $iNdex               - an integer value.
;                  $sObject             - a string value.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _SQLquery($sTable, $iNdex, $sObject)
	Local $Region = "Region - SQL Query", $aDBout

	If $bVerbose Then _Log($Region & ", Info,SQLQuery activated.")

	If $bVerbose Then _Log($Region & ", Info, Qurey if " & $iNdex & " exist in table: " & $sTable & " for: " & $sObject)
	_SQLite_QuerySingleRow($hDB, "SELECT " & $iNdex & " FROM " & $sTable & " WHERE DName='" & $sObject & "'", $aDBout)

	If @error And _SQLite_ErrCode() <> 0 Then
		If $bError Then _Log($Region & ", Error, Query Error - Dname:" & $sObject & " Object:" & $iNdex)
		If $bError Then _Log($Region & ", Error, SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
	EndIf

	If $aDBout[0] <> "" Then
;~ 		If $bDEBUG Then _DebugArrayDisplay($aDBout, "aDBout - SQLquery")
		If $bVerbose Then _Log($Region & ", Info, Object found in DB: " & $aDBout[0])
		Return $aDBout[0]
	Else
		If $bVerbose Then _Log($Region & ", Info, Object NOT found in DB: " & $aDBout[0])
		Return $sG_NoExist
	EndIf

EndFunc   ;==>_SQLquery

;Delete from DB
; #FUNCTION# ====================================================================================================================
; Name ..........: _SQLdel
; Description ...:
; Syntax ........: _SQLdel($sTable, $iNdex, $sObject, $sDName)
; Parameters ....: $sTable              - a string value.
;                  $iNdex               - an integer value.
;                  $sObject             - a string value.
;                  $sDName              - a string value.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _SQLdel($sTable, $iNdex, $sObject, $sDName)
	;"DName,LKnownIP,AddDate,LKnownRegDate,LknownRegStat,CRegStat"
	Local $Region = "Region - SQL Del", $aDBout, $sReturn
	_Log($Region & ", Global, SQLDel activated.")

	If $bVerbose Then _Log($Region & ", Info, Qurey if " & $sObject & " exist in table: " & $sTable & " for: " & $iNdex)
	_SQLite_QuerySingleRow($hDB, "SELECT " & $iNdex & " FROM " & $sTable & " WHERE " & $iNdex & "='" & $sObject & "'", $aDBout)
	If @error Then
		If $bError Then _Log($Region & ", Error, Query Error - Dname:" & $sObject & " Object:" & $iNdex)
		If $bError Then _Log($Region & ", Error, SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
	EndIf

	If $aDBout[0] <> "" Then
		If $bVerbose Then _Log($Region & ", Info,Removing device: " & @CRLF & $sDName)

		$sReturn = _SQLite_Exec($hDB, "DELETE FROM status WHERE DName='" & $sDName & "'")
		If @error Then
			If $bError Then _Log($Region & ", Error, Query Error - Dname:" & $sObject & " Object:" & $iNdex)
			If $bError Then _Log($Region & ", Error, SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
		EndIf
	EndIf
	_Log($sReturn)
EndFunc   ;==>_SQLdel

;Shutdown DB
; #FUNCTION# ====================================================================================================================
; Name ..........: _Shutdown
; Description ...:
; Syntax ........: _Shutdown()
; Parameters ....:
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _Shutdown($exitCode = 0)
	Local $Region = "Region - SQL Shutdown"
	_Log($Region & ", Global,Closing DB.")
	_SQLite_Close()

	_Log($Region & ", Global,Shutting down SQLite.")
	_SQLite_Shutdown()

	If $exitCode = 0 Then
		_Log($Region & ", Global, Successful completion. Exiting system.")
	Else
		_Log($Region & ", Global, Exiting system with errors.")
	EndIf
	Exit $exitCode
EndFunc   ;==>_Shutdown

#EndRegion - SQL

#Region - SOAP

; #FUNCTION# ====================================================================================================================
; Name ..........: _ListPhone
; Description ...:
; Syntax ........: _ListPhone()
; Parameters ....:
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _ListPhone()
	Local $Region = "Region - List Phone", $aXML, $sSOAP, $fName = $sFilerDir & "\list_all.tmp"
	_Log($Region & ", Global,ListPhone activated.")

	_Log($Region & ", Global,Initiating XML Formatter.")
	$aXML = _XML_Formatter("10", $sHostName_IP_Port, $sAuthorization, "listPhone", "%")
	If $bDEBUG Then _DebugArrayDisplay($aXML, "$aXML")

	_Log($Region & ", Global, Initiating SOAP call.")
	$sSOAP = _SOAP_Open($aXML[0], $aXML[1], $aXML[2], $aXML[3], $aXML[4], $aXML[5])

	Return $sSOAP
EndFunc   ;==>_ListPhone

; #FUNCTION# ====================================================================================================================
; Name ..........: _SelectCMDevices
; Description ...:
; Syntax ........: _SelectCMDevices($sTable, $sIndex)
; Parameters ....: $sTable              - a string value.
;                  $sIndex              - a string value.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _SelectCMDevices() ;Receive batch array
	Local $Region = "Region - SelectCMDevices", $aXML, $sSOAP, $fName, $hTimer, $tDiff, $iRows, $sResult, $iSleeper = 20000, $sReturn, $aArray, $iCols
	Local Static	$iRetry = 0
	_Log($Region & ", Global, SelectCMDevices activated.")

	$sReturn = _SQLite_GetTable($hDB, "SELECT " & $aTables[$iBatch][$iColumns] & " FROM " & $aTables[$iBatch][$iName] & ";", $aArray, $iRows, $iCols)
	If $sReturn = $SQLITE_OK Then
		_Log($Region & ", Global, Rows: " & $iRows & @CRLF & "Columns: " & $iCols & @CRLF)
		_ArrayDelete($aArray, "0-1")

		If $bDEBUG Then _DebugArrayDisplay($aArray)

	Else
		_Log($Region & ", Error, SQLite Error: " & $sReturn, _SQLite_ErrMsg())
		Return 'Error'
	EndIf

	If $bDEBUG Then _DebugArrayDisplay($aArray, "Batch array in SelectCMDevices")

	For $i = 0 To $iRows - 1
		;If $i = 1 Then _Shutdown()
		If $iRetry = 3 Then
			_Log($Region & ",Error, SOAP calls has reached maximum retries for this batch. Skipping to next batch..")
			ContinueLoop
		EndIf

		$fName = $sFilerDir & "\rawdata_" & $i & ".xml" ;Create name based on batch number

		_Log($Region & ", Global,Initiating XML Formatter.")
		_Log($Region & ", Global, Encapsulating: " & $aArray[$i])
;~ 		$aXML = _XML_FormatterRIS($sCUCM_Ver, $sHostName_IP_Port, $sAuthorization, "SelectCmDevice", $iDevCount, $sClass, $sDevStatus, $sSelectDevBy, $aArray[$i])
		$aXML = _XML_FormatterRIS($sCUCM_Ver, $sHostName_IP_Port, $sAuthorization, "SelectCmDevice", 1000, $sClass, $sDevStatus, $sSelectDevBy, $aArray[$i]) ; Changed return to max because we were missing returns.
		If $bDEBUG Then _DebugArrayDisplay($aXML, "$aXML")

		If $bVerbose Then _Log($Region & ", Info,Initiating SOAP call.")
		If $aXML[5] = "" Then
			If $bError Then _Log($Region & ", Error, The SOAP Envelope appears to be blank.")
			Return 'Error'
		EndIf
		$sSOAP = _SOAP_Open($aXML[0], $aXML[1], $aXML[2], $aXML[3], $aXML[4], $aXML[5]) ;Send the exact devices names to status call.

		$sResult = _Filer($fName, True, $sSOAP)
		If FileExists($sResult) Then ;Write XML return to file
			_Log($Region & ", Global, Successfully created: " & $fName)
		Else
			If $bError Then _Log($Region & ", Error, Failed to create: " & $fName)
			Return "Error" ;If this fails then initialization has failed and will need to start over.
		EndIf

		$hTimer = TimerInit()

		$sResult = _SelectCMDevices_Processor($fName, $i)
		If $sResult = "Good" Then ; Send the batch number for the naming process
			If $iSleeper > 20000 Then
				$iSleeper = 20000
				If $bError or $bVerbose Then _Log($Region & ", Info,Last retrieval was successful. Resetting time between retrievals to 20 seconds. Retrieval Timer: " & $iSleeper)
			EndIf
			$tDiff = TimerDiff($hTimer)
			If $bVerbose Then _Log($Region & ", Info,Timer 1: " & $tDiff)
			If $tDiff < $iSleeper Then ;wait a total of 20 (or more) seconds before sending followup query. This is because there's a 15 request per minute limitation in CUCM.
				Do
					Sleep(10)
				Until TimerDiff($hTimer) >= $iSleeper
			EndIf
			If $bError or $bVerbose Then _Log($Region & ", Info,Timer 2: " & $tDiff)

		Elseif $sResult = "Retry" Then
			$i -= 1
			$iRetry += 1
			$tDiff = TimerDiff($hTimer)
			$iSleeper += 10000
			If $bError Then _Log($Region & ", Error,Last retrieval was empty or errored. Increasing time between retrievals by 10 seconds. Retrieval Timer: " & $iSleeper)
			If $bVerbose Then _Log($Region & ", Info,Timer 1: " & $tDiff)
			If $tDiff < $iSleeper Then
				Do
					Sleep(10)
				Until TimerDiff($hTimer) >= $iSleeper
			EndIf
			If $bVerbose Then _Log($Region & ", Info,Timer 2: " & $tDiff)

		ElseIf $sResult = "Error" Then
				Return "Error"
		EndIf
	Next

	Return "Complete"

EndFunc   ;==>_SelectCMDevices

#EndRegion - SOAP

#Region - XML

;XMLWrapperEx provided by mLipok

; #FUNCTION# ====================================================================================================================
; Name ..........: _SelectNodes
; Description ...:
; Syntax ........: _SelectNodes($sNode, $sXmlFile)
; Parameters ....: $sNode               - a string value.
;                  $sXmlFile            - a string value.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _SelectNodes($sNode, $sXmlFile)
	Local $Region = "Region - SelectNodes", $aNodesColl
	_Log($Region & ", Global, Node Selector - FileName: " & $sXmlFile)
	; first you must create $oXmlDoc object
	Local $oXMLDoc = _XML_CreateDOMDocument(Default)
	If @error Then
		If $bError Then _Log($Region & ", Error,_XML_CreateDOMDocument @error: " & XML_My_ErrorParser(@error))
	Else
		; now you can add EVENT Handler
		Local $oXMLDOM_EventsHandler = ObjEvent($oXMLDoc, "XML_DOM_EVENT_")
		#forceref $oXMLDOM_EventsHandler

		; Load file to $oXmlDoc
		_XML_Load($oXMLDoc, $sXmlFile)
		If @error Then
			If $bError Then _Log($Region & ", Error,_XML_Load @error: " & XML_My_ErrorParser(@error))
		Else

			; simple display $oXmlDoc - for checking only
			Local $sXmlAfterTidy = _XML_TIDY($oXMLDoc)
			If @error Then
				If $bError Then _Log($Region & ", Error,_XML_TIDY @error: " & XML_My_ErrorParser(@error))
			EndIf

			; selecting nodes
			Local $oNodesColl = _XML_SelectNodes($oXMLDoc, $sNode)
			If @error Then
				If $bError Then _Log($Region & ", Error,_XML_SelectNodes @error: " & XML_My_ErrorParser(@error))
			Else
				; change Nodes Collection to array
				Dim $aNodesColl = _XML_Array_GetNodesProperties($oNodesColl)
				If @error Then
					If $bError Then _Log($Region & ", Error,_XML_Array_GetNodesProperties @error: " & XML_My_ErrorParser(@error))
				Else
					; display array
					If $bDEBUG Then _DebugArrayDisplay($aNodesColl, "_XML_SelectNodes : " & 'Length=' & $oNodesColl.length & '   XPath=' & $oNodesColl.expr)
				EndIf
			EndIf
		EndIf
	EndIf

	If Not IsArray($aNodesColl) Then
		If $bError Then _Log($Region & ", Error,The Node array is empty.")
		Return "Empty"
	EndIf

	If $bVerbose Then _Log($Region & ", Info,Node selector complete for: " & $sNode)

	Return $aNodesColl

EndFunc   ;==>_SelectNodes

; #FUNCTION# ====================================================================================================================
; Name ..........: ErrFunc_CustomUserHandler_XML
; Description ...:
; Syntax ........: ErrFunc_CustomUserHandler_XML($oError)
; Parameters ....: $oError              - an object.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func ErrFunc_CustomUserHandler_XML($oError)

	; here is declared another path to UDF au3 file
	; thanks to this with using _XML_ComErrorHandler_UserFunction(ErrFunc_CustomUserHandler_XML)
	;  you get errors which after pressing F4 in SciTE4AutoIt you goes directly to the specified UDF Error Line
	_Log(@ScriptDir & "\XMLWrapperEx.au3" & " (" & $oError.scriptline & ") : UDF ==> COM Error intercepted ! " & @CRLF & _
			@TAB & "err.number is: " & @TAB & @TAB & "0x" & Hex($oError.number) & @CRLF & _
			@TAB & "err.windescription:" & @TAB & $oError.windescription & @CRLF & _
			@TAB & "err.description is: " & @TAB & $oError.description & @CRLF & _
			@TAB & "err.source is: " & @TAB & @TAB & $oError.source & @CRLF & _
			@TAB & "err.helpfile is: " & @TAB & $oError.helpfile & @CRLF & _
			@TAB & "err.helpcontext is: " & @TAB & $oError.helpcontext & @CRLF & _
			@TAB & "err.lastdllerror is: " & @TAB & $oError.lastdllerror & @CRLF & _
			@TAB & "err.scriptline is: " & @TAB & $oError.scriptline & @CRLF & _
			@TAB & "err.retcode is: " & @TAB & "0x" & Hex($oError.retcode) & @CRLF)
EndFunc   ;==>ErrFunc_CustomUserHandler_XML

; #FUNCTION# ====================================================================================================================
; Name ..........: XML_My_ErrorParser
; Description ...:
; Syntax ........: XML_My_ErrorParser($iXMLWrapper_Error[, $iXMLWrapper_Extended = 0])
; Parameters ....: $iXMLWrapper_Error   - an integer value.
;                  $iXMLWrapper_Extended- [optional] an integer value. Default is 0.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func XML_My_ErrorParser($iXMLWrapper_Error, $iXMLWrapper_Extended = 0)
	Local $sErrorInfo = ""
	Switch $iXMLWrapper_Error
		Case $XML_ERR_SUCCESS
			$sErrorInfo = "$XML_ERR_SUCCESS=" & $XML_ERR_SUCCESS & @CRLF & "All is ok."
		Case $XML_ERR_GENERAL
			$sErrorInfo = "$XML_ERR_GENERAL=" & $XML_ERR_GENERAL & @CRLF & "The error which is not specifically defined."
		Case $XML_ERR_COMERROR
			$sErrorInfo = "$XML_ERR_COMERROR=" & $XML_ERR_COMERROR & @CRLF & "COM ERROR OCCURED. Check @extended and your own error handler function for details."
		Case $XML_ERR_ISNOTOBJECT
			$sErrorInfo = "$XML_ERR_ISNOTOBJECT=" & $XML_ERR_ISNOTOBJECT & @CRLF & "No object passed to function"
		Case $XML_ERR_INVALIDDOMDOC
			$sErrorInfo = "$XML_ERR_INVALIDDOMDOC=" & $XML_ERR_INVALIDDOMDOC & @CRLF & "Invalid object passed to function"
		Case $XML_ERR_INVALIDATTRIB
			$sErrorInfo = "$XML_ERR_INVALIDATTRIB=" & $XML_ERR_INVALIDATTRIB & @CRLF & "Invalid object passed to function."
		Case $XML_ERR_INVALIDNODETYPE
			$sErrorInfo = "$XML_ERR_INVALIDNODETYPE=" & $XML_ERR_INVALIDNODETYPE & @CRLF & "Invalid object passed to function."
		Case $XML_ERR_OBJCREATE
			$sErrorInfo = "$XML_ERR_OBJCREATE=" & $XML_ERR_OBJCREATE & @CRLF & "Object can not be created."
		Case $XML_ERR_NODECREATE
			$sErrorInfo = "$XML_ERR_NODECREATE=" & $XML_ERR_NODECREATE & @CRLF & "Can not create Node - check also COM Error Handler"
		Case $XML_ERR_NODEAPPEND
			$sErrorInfo = "$XML_ERR_NODEAPPEND=" & $XML_ERR_NODEAPPEND & @CRLF & "Can not append Node - check also COM Error Handler"
		Case $XML_ERR_PARSE
			$sErrorInfo = "$XML_ERR_PARSE=" & $XML_ERR_PARSE & @CRLF & "Error: with Parsing objects, .parseError.errorCode=" & $iXMLWrapper_Extended & " Use _XML_ErrorParser_GetDescription() for get details."
		Case $XML_ERR_PARSE_XSL
			$sErrorInfo = "$XML_ERR_PARSE_XSL=" & $XML_ERR_PARSE_XSL & @CRLF & "Error with Parsing XSL objects .parseError.errorCode=" & $iXMLWrapper_Extended & " Use _XML_ErrorParser_GetDescription() for get details."
		Case $XML_ERR_LOAD
			$sErrorInfo = "$XML_ERR_LOAD=" & $XML_ERR_LOAD & @CRLF & "Error opening specified file."
		Case $XML_ERR_SAVE
			$sErrorInfo = "$XML_ERR_SAVE=" & $XML_ERR_SAVE & @CRLF & "Error saving file."
		Case $XML_ERR_PARAMETER
			$sErrorInfo = "$XML_ERR_PARAMETER=" & $XML_ERR_PARAMETER & @CRLF & "Wrong parameter passed to function."
		Case $XML_ERR_ARRAY
			$sErrorInfo = "$XML_ERR_ARRAY=" & $XML_ERR_ARRAY & @CRLF & "Wrong array parameter passed to function. Check array dimension and conent."
		Case $XML_ERR_XPATH
			$sErrorInfo = "$XML_ERR_XPATH=" & $XML_ERR_XPATH & @CRLF & "XPath syntax error - check also COM Error Handler."
		Case $XML_ERR_NONODESMATCH
			$sErrorInfo = "$XML_ERR_NONODESMATCH=" & $XML_ERR_NONODESMATCH & @CRLF & "No nodes match the XPath expression"
		Case $XML_ERR_NOCHILDMATCH
			$sErrorInfo = "$XML_ERR_NOCHILDMATCH=" & $XML_ERR_NOCHILDMATCH & @CRLF & "There is no Child in nodes matched by XPath expression."
		Case $XML_ERR_NOATTRMATCH
			$sErrorInfo = "$XML_ERR_NOATTRMATCH=" & $XML_ERR_NOATTRMATCH & @CRLF & "There is no such attribute in selected node."
		Case $XML_ERR_DOMVERSION
			$sErrorInfo = "$XML_ERR_DOMVERSION=" & $XML_ERR_DOMVERSION & @CRLF & "DOM Version: " & "MSXML Version " & $iXMLWrapper_Extended & " or greater required for this function"
		Case $XML_ERR_EMPTYCOLLECTION
			$sErrorInfo = "$XML_ERR_EMPTYCOLLECTION=" & $XML_ERR_EMPTYCOLLECTION & @CRLF & "Collections of objects was empty"
		Case $XML_ERR_EMPTYOBJECT
			$sErrorInfo = "$XML_ERR_EMPTYOBJECT=" & $XML_ERR_EMPTYOBJECT & @CRLF & "Object is empty"
		Case Else
			$sErrorInfo = "=" & $iXMLWrapper_Error & @CRLF & "NO ERROR DESCRIPTION FOR THIS @error"
	EndSwitch

	Local $sExtendedInfo = ""
	Switch $iXMLWrapper_Error
		Case $XML_ERR_COMERROR, $XML_ERR_NODEAPPEND, $XML_ERR_NODECREATE
			$sExtendedInfo = "COM ERROR NUMBER (@error returned via @extended) =" & $iXMLWrapper_Extended
		Case $XML_ERR_PARAMETER
			$sExtendedInfo = "This @error was fired by parameter: #" & $iXMLWrapper_Extended
		Case Else
			Switch $iXMLWrapper_Extended
				Case $XML_EXT_DEFAULT
					$sExtendedInfo = "$XML_EXT_DEFAULT=" & $XML_EXT_DEFAULT & @CRLF & "Default - Do not return any additional information"
				Case $XML_EXT_XMLDOM
					$sExtendedInfo = "$XML_EXT_XMLDOM=" & $XML_EXT_XMLDOM & @CRLF & '"Microsoft.XMLDOM" related Error'
				Case $XML_EXT_DOMDOCUMENT
					$sExtendedInfo = "$XML_EXT_DOMDOCUMENT=" & $XML_EXT_DOMDOCUMENT & @CRLF & '"Msxml2.DOMDocument" related Error'
				Case $XML_EXT_XSLTEMPLATE
					$sExtendedInfo = "$XML_EXT_XSLTEMPLATE=" & $XML_EXT_XSLTEMPLATE & @CRLF & '"Msxml2.XSLTemplate" related Error'
				Case $XML_EXT_SAXXMLREADER
					$sExtendedInfo = "$XML_EXT_SAXXMLREADER=" & $XML_EXT_SAXXMLREADER & @CRLF & '"MSXML2.SAXXMLReader" related Error'
				Case $XML_EXT_MXXMLWRITER
					$sExtendedInfo = "$XML_EXT_MXXMLWRITER=" & $XML_EXT_MXXMLWRITER & @CRLF & '"MSXML2.MXXMLWriter" related Error'
				Case $XML_EXT_FREETHREADEDDOMDOCUMENT
					$sExtendedInfo = "$XML_EXT_FREETHREADEDDOMDOCUMENT=" & $XML_EXT_FREETHREADEDDOMDOCUMENT & @CRLF & '"Msxml2.FreeThreadedDOMDocument" related Error'
				Case $XML_EXT_XMLSCHEMACACHE
					$sExtendedInfo = "$XML_EXT_XMLSCHEMACACHE=" & $XML_EXT_XMLSCHEMACACHE & @CRLF & '"Msxml2.XMLSchemaCache." related Error'
				Case $XML_EXT_STREAM
					$sExtendedInfo = "$XML_EXT_STREAM=" & $XML_EXT_STREAM & @CRLF & '"ADODB.STREAM" related Error'
				Case $XML_EXT_ENCODING
					$sExtendedInfo = "$XML_EXT_ENCODING=" & $XML_EXT_ENCODING & @CRLF & 'Encoding related Error'
				Case Else
					$sExtendedInfo = "$iXMLWrapper_Extended=" & $iXMLWrapper_Extended & @CRLF & 'NO ERROR DESCRIPTION FOR THIS @extened'
			EndSwitch
	EndSwitch
	; return back @error and @extended for further debuging
	Return SetError($iXMLWrapper_Error, $iXMLWrapper_Extended, _
			"@error description:" & @CRLF & _
			$sErrorInfo & @CRLF & _
			@CRLF & _
			"@extended description:" & @CRLF & _
			$sExtendedInfo & @CRLF & _
			"")

EndFunc   ;==>XML_My_ErrorParser

; #FUNCTION# ====================================================================================================================
; Name ..........: ErrFunc_CustomUserHandler_MAIN
; Description ...:
; Syntax ........: ErrFunc_CustomUserHandler_MAIN($oError)
; Parameters ....: $oError              - an object.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func ErrFunc_CustomUserHandler_MAIN($oError)
	_Log(@ScriptName & " (" & $oError.scriptline & ") : MainScript ==> COM Error intercepted !" & @CRLF & _
			@TAB & "err.number is: " & @TAB & @TAB & "0x" & Hex($oError.number) & @CRLF & _
			@TAB & "err.windescription:" & @TAB & $oError.windescription & @CRLF & _
			@TAB & "err.description is: " & @TAB & $oError.description & @CRLF & _
			@TAB & "err.source is: " & @TAB & @TAB & $oError.source & @CRLF & _
			@TAB & "err.helpfile is: " & @TAB & $oError.helpfile & @CRLF & _
			@TAB & "err.helpcontext is: " & @TAB & $oError.helpcontext & @CRLF & _
			@TAB & "err.lastdllerror is: " & @TAB & $oError.lastdllerror & @CRLF & _
			@TAB & "err.scriptline is: " & @TAB & $oError.scriptline & @CRLF & _
			@TAB & "err.retcode is: " & @TAB & "0x" & Hex($oError.retcode) & @CRLF)
EndFunc   ;==>ErrFunc_CustomUserHandler_MAIN

; #FUNCTION# ====================================================================================================================
; Name ..........: XML_ObjName_FlagsValue
; Description ...:
; Syntax ........: XML_ObjName_FlagsValue(Byref $oObj)
; Parameters ....: $oObj                - [in/out] an object.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func XML_ObjName_FlagsValue(ByRef $oObj)
	Local $Region = 'Region - XML ObjName', $sInfo = ""

	$sInfo &= "+>" & @TAB & "ObjName($oObj,1) {The name of the Object} =" & @CRLF & @TAB & ObjName($oObj, $OBJ_NAME) & @CRLF

	; HELPFILE REMARKS: Not all Objects support flags 2 to 7. Always test for @error in these cases.
	$sInfo &= "+>" & @TAB & "ObjName($oObj,2) {Description string of the Object} =" & @CRLF & @TAB & ObjName($oObj, $OBJ_STRING)
	If @error Then $sInfo &= "@error = " & @error
	$sInfo &= @CRLF & @CRLF

	$sInfo &= "+>" & @TAB & "ObjName($oObj,3) {The ProgID of the Object} =" & @CRLF & @TAB & ObjName($oObj, $OBJ_PROGID)
	If @error Then $sInfo &= "@error = " & @error
	$sInfo &= @CRLF & @CRLF

	$sInfo &= "+>" & @TAB & "ObjName($oObj,4) {The file that is associated with the object in the Registry} =" & @CRLF & @TAB & ObjName($oObj, $OBJ_FILE)
	If @error Then $sInfo &= "@error = " & @error
	$sInfo &= @CRLF & @CRLF

	$sInfo &= "+>" & @TAB & "ObjName($oObj,5) {Module name in which the object runs (WIN XP And above). Marshaller for non-inproc objects.} =" & @CRLF & @TAB & ObjName($oObj, $OBJ_MODULE)
	If @error Then $sInfo &= "@error = " & @error
	$sInfo &= @CRLF & @CRLF

	$sInfo &= "+>" & @TAB & "ObjName($oObj,6) {CLSID of the object""s coclass} =" & @CRLF & @TAB & ObjName($oObj, $OBJ_CLSID)
	If @error Then $sInfo &= "@error = " & @error
	$sInfo &= @CRLF & @CRLF

	$sInfo &= "+>" & @TAB & "ObjName($oObj,7) {IID of the object""s interface} =" & @CRLF & @TAB & ObjName($oObj, $OBJ_IID)
	If @error Then $sInfo &= "@error = " & @error
	$sInfo &= @CRLF & @CRLF

	_Log($Region & ",Info, ObjName:", $sInfo)
EndFunc   ;==>XML_ObjName_FlagsValue

#EndRegion - XML

#Region - Processors

; #FUNCTION# ====================================================================================================================
; Name ..........: _ListPhone_Processor
; Description ...:
; Syntax ........: _ListPhone_Processor()
; Parameters ....:
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _ListPhone_Processor()
	Local $Region = "Region - List Phone Processor", $hFile, $sFile, $aModels, $iRows, $aRemKeys, $iRowsRem, $sRtnProcessor, $aArray, $sReturn, $aTemp, $iR, $iC
	_Log($Region & ", Global, ListPhone Processing activated.")

	$sFile = _ListPhone()
	If $sFile = "Error" Then
		If $bError Then _Log($Region & ", Error, RetrieveDevices returned: " & $sFile & ". Terminating further operations.")
		Return 'Error'
	Else
		If $bVerbose Then _Log($Region & ", Info,RetrieveDevices returned: " & $sFile)
	EndIf

	$aArray = _StringBetween($sFile, "<name>", "</name>")

	If @error Then
		If $bError Then _Log($Region & ", Error, No devices found")
		Return "Error"
	EndIf

	If $bDEBUG Then _DebugArrayDisplay($aArray, "$aArray")

	$iRows = UBound($aArray, $UBOUND_ROWS)

	_ArraySort($aArray, 0, Default, Default, 1)

	$aRemKeys = _Type_Processor($aArray, "Phone")

	$iRowsRem = UBound($aRemKeys)

	If $iRowsRem < 1 Then
		If $bVerbose Then _Log($Region & ", Info, aRemKey is empty. Skipping purge:" & $aRemKeys)
	ElseIf $iRowsRem = 1 Then
		$aRemKeys = $aRemKeys[0]
		If $bVerbose Then _Log($Region & ", Info, aRemKey re-initialized as an int:" & $aRemKeys)
		$sReturn = _Purge_Processor($aArray, $aRemKeys)
	Else
		$sReturn = _Purge_Processor($aArray, $aRemKeys)
	EndIf

	If StringInStr($sReturn, 'Error') Then Return "Error"
	If $bDEBUG Then _DebugArrayDisplay($aArray, $sG_TBuilt)

	#Region - Begin Transaction for 'built'
	$sRtnProcessor = _SQL_BnB_Processor($aTables[$iBuilt][$iName], $aTables[$iBuilt][$iColumns], $aArray) ; Add all devices to the built table
	If StringInStr("Error", $sRtnProcessor) Then
		_Log($Region & ",Error ,Error in SQL " & $aTables[$iBuilt][$iName] & " processor function. Terminating program to protect DB.")
		Return "Error"
	Else
		_Log($Region & ",Info , Successfully UPDATED '"&$aTables[$iBuilt][$iName]&"' table.")
	EndIf
	#EndRegion - COMMIT Transaction for 'built'

	#Region - Begin Transaction for 'devices'
	$sRtnProcessor = _SQL_BnB_Processor($aTables[$iDevices][$iName], $aTables[$iDevices][$iColumns], $aArray) ; Add all devices to the devices table
	If StringInStr("Error", $sRtnProcessor) Then
		_Log($Region & ",Error ,Error in SQL " & $aTables[$iDevices][$iName] & " processor function. Terminating program to protect DB.")
		Return "Error"
	Else
		_Log($Region & ",Info , Successfully UPDATED '"&$aTables[$iDevices][$iName]&"' table.")
	EndIf

	#cs
	$sRtnProcessor = _SQLaddBnB($aTables[$iDevices][$iName], $sG_ModTracker, '0') ; Update ModTracker to 0 for all devices
	If StringInStr("Error", $sRtnProcessor) Then
		_Log($Region & ",Error ,Error in SQL " & $aTables[$iDevices][$iName] & " processor function. Terminating program to protect DB.")
		_Shutdown()
	Else
		_Log($Region & ",Info , Successfully UPDATED '"&$aTables[$iDevices][$iName]&"' table.")
	EndIf
	#ce
	#EndRegion - COMMIT Transaction for 'devices'

	#Region - Begin Transaction for 'mod'
	$sRtnProcessor = _SQL_BnB_Processor($aTables[$iMod][$iName], $sG_DName, $aArray) ; Add all devices to the mod table
	If StringInStr("Error", $sRtnProcessor) Then
		_Log($Region & ",Error ,Error in SQL " & $aTables[$iMod][$iName] & " processor function. Terminating program to protect DB.")
		Return "Error"
	Else
		_Log($Region & ",Info , Successfully UPDATED '"&$aTables[$iMod][$iName]&"' table.")
	EndIf
	#cs
	$sRtnProcessor = _SQLaddBnB($aTables[$iMod][$iName], "'"&$dNow&"'", "FALSE") ; Update Today's instance to 0 for all devices
	If StringInStr("Error", $sRtnProcessor) Then
		_Log($Region & ",Error ,Error in SQL " & $aTables[$iMod][$iName] & " processor function. Terminating program to protect DB.")
		Return "Error"
	Else
		_Log($Region & ",Info , Successfully UPDATED '"&$aTables[$iMod][$iName]&"' table.")
	EndIf
	#ce
	#EndRegion - COMMIT Transaction for 'mod'

	Return 'Good'
EndFunc   ;==>_ListPhone_Processor

; #FUNCTION# ====================================================================================================================
; Name ..........: _SelectCMDevices_Processor
; Description ...:
; Syntax ........: _SelectCMDevices_Processor($fName, $iNdex)
; Parameters ....: $fName               - a floating point value.
;                  $iNdex               - an integer value.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _SelectCMDevices_Processor($fName, $iNdex)
	Local $Region = "Region - SelectCMDevices Processor", $aArray, $sXMLNode, $iDevicesReturned, $aCMNodes, $aCMDevices, $aCMIPs, $aCMStatus, $aCMEpoch, $sFiler, $aList, $sRtnProcessor
	Local Static $iRetry = 0
	_Log($Region & ", Global, SelectCMDevice Processing activated.")

	#Region - Get Device Count
	$sXMLNode = "//TotalDevicesFound"
	If $bVerbose Then _Log($Region & ", Info, Search node path: " & $sXMLNode)
	$aArray = _SelectNodes($sXMLNode, $fName)
	If $aArray = "Empty" Then
		If $bError Then _Log($Region & ", Error, Empty return from Node Selector")
		Return "Retry"
	EndIf

	If $bVerbose Then _Log($Region & ", Info, Node search returned")
	$aArray = _ArrayCleanup($aArray, 3, 3, 1)
	$iDevicesReturned = $aArray[0]
	$aArray = Null
	If $bVerbose Then _Log($Region & ", Info, Total devices found: " & $iDevicesReturned)
	If $iDevicesReturned = 0 Then
		$iRetry += 1
		If $bError Then _Log($Region & ", Error, Retrying SOAP retrieval")
		Return "Retry"
	Else
		$iRetry = 0
	EndIf

	#EndRegion - Get Device Count

	#Region - Get CMNodes
	$sXMLNode = "//CmNodes/item/Name"
	If $bVerbose Then _Log($Region & ", Info, Search node path: " & $sXMLNode)
	$aArray = _SelectNodes($sXMLNode, $fName)
	If $aArray = "Empty" Then
		If $bError Then _Log($Region & ", Error, Empty return from Node Selector")
		If $bError Then _Log($Region & ", Error, Retrying SOAP retrieval")
		$iRetry += 1
		Return "Retry"
	EndIf

	If $bVerbose Then _Log($Region & ", Info, Node search returned")
	$aArray = _ArrayCleanup($aArray, 3, 3, 1)
	$aCMNodes = $aArray
	$aArray = Null
	For $cmnode In $aCMNodes
		If $bVerbose Then _Log($Region & ", Info, Found Node: " & $cmnode)
	Next


	#EndRegion - Get CMNodes

	#Region - Get Deviec Names
	$sXMLNode = "//CmDevices/item/Name"
	If $bVerbose Then _Log($Region & ", Info, Search node path: " & $sXMLNode)
	$aArray = _SelectNodes($sXMLNode, $fName)
	If $aArray = "Empty" Then
		If $bError Then _Log($Region & ", Error, Empty return from Node Selector")
		If $bError Then _Log($Region & ", Error, Retrying SOAP retrieval")
		$iRetry += 1
		Return "Retry"
	EndIf

	If $bVerbose Then _Log($Region & ", Info, Node search returned")
	$aArray = _ArrayCleanup($aArray, 3, 3, 1)
	$aCMDevices = $aArray
	$aArray = Null
	For $cmnode In $aCMDevices
		If $bVerbose Then _Log($Region & ", Info, Found CM Device: " & $cmnode)
	Next

	#EndRegion - Get Deviec Names

	#Region - Get Device IP

	$sXMLNode = "//CmDevices/item/IpAddress"
	If $bVerbose Then _Log($Region & ", Info, Search node path: " & $sXMLNode)
	$aArray = _SelectNodes($sXMLNode, $fName)
	If $aArray = "Empty" Then
		If $bError Then _Log($Region & ", Error, Empty return from Node Selector")
		If $bError Then _Log($Region & ", Error, Retrying SOAP retrieval")
		$iRetry += 1
		Return "Retry"
	EndIf

	If $bVerbose Then _Log($Region & ", Info, Node search returned")
	$aArray = _ArrayCleanup($aArray, 3, 3, 1)
	$aCMIPs = $aArray
	$aArray = Null
	For $cmnode In $aCMIPs
		If $bVerbose Then _Log($Region & ", Info, Found CM IP: " & $cmnode)
	Next

	#EndRegion - Get Device IP

	#Region - Registration Status

	$sXMLNode = "//CmDevices/item/Status"
	If $bVerbose Then _Log($Region & ", Info, Search node path: " & $sXMLNode)
	$aArray = _SelectNodes($sXMLNode, $fName)
	If $aArray = "Empty" Then
		If $bError Then _Log($Region & ", Error, Empty return from Node Selector")
		If $bError Then _Log($Region & ", Error, Retrying SOAP retrieval")
		$iRetry += 1
		Return "Retry"
	EndIf

	If $bVerbose Then _Log($Region & ", Info, Node search returned")
	$aArray = _ArrayCleanup($aArray, 3, 3, 1)
	$aCMStatus = $aArray
	$aArray = Null
	For $cmnode In $aCMStatus
		If $bVerbose Then _Log($Region & ", Info, Found CM Status: " & $cmnode)
	Next

	#EndRegion - Registration Status

	#Region - Registration Status

	$sXMLNode = "//CmDevices/item/TimeStamp"
	If $bVerbose Then _Log($Region & ", Info, Search node path: " & $sXMLNode)
	$aArray = _SelectNodes($sXMLNode, $fName)
	If $aArray = "Empty" Then
		If $bError Then _Log($Region & ", Error, Empty return from Node Selector")
		If $bError Then _Log($Region & ", Error, Retrying SOAP retrieval")
		$iRetry += 1
		Return "Retry"
	EndIf

	If $bVerbose Then _Log($Region & ", Info, Node search returned")
	$aArray = _ArrayCleanup($aArray, 3, 3, 1)
	$aCMEpoch = $aArray
	$aArray = Null
	For $cmnode In $aCMEpoch
		If $bVerbose Then _Log($Region & ", Info, Found CM Epoch: " & $cmnode)
	Next

	#EndRegion - Registration Status

	#Region - Concatenate
	Dim $aArray[$iDevicesReturned][4]

	For $x = 0 To $iDevicesReturned - 1
		$aArray[$x][0] = $aCMDevices[$x]
		$aArray[$x][1] = $aCMIPs[$x]
		$aArray[$x][2] = $aCMStatus[$x]
		$aArray[$x][3] = $aCMEpoch[$x]
;~ 		$aArray[$x][3] = $aCMNodes
		If $bVerbose Then _Log($Region & ", Info, Concatenated: " & $aArray[$x][0] & "," & $aArray[$x][1] & "," & $aArray[$x][2] & "," & $aArray[$x][3])
	Next

	;Don't forget the DUPE check. Some may have been registered to multiple devices. Remeber registered always trumps unregistered
	If $bDEBUG Then _DebugArrayDisplay($aArray, "CM")
	#EndRegion - Concatenate


	$sRtnProcessor = _SQL_Dev_Processor($aTables[$iDevices][$iName], $aTables[$iDevices][$iColumns], $aArray)
	If StringInStr("Error", $sRtnProcessor) Then
		_Log($Region & ",Error ,Error in SQL Dev processor function. Terminating program to protect DB.")
		Return "Error"
	EndIf

	Return "Good"
EndFunc   ;==>_SelectCMDevices_Processor

 _Unknown_Processor
; #FUNCTION# ====================================================================================================================
; Name ..........: _Unknown_Processor
; Description ...:
; Syntax ........: _Unknown_Processor($sTable)
; Parameters ....: $sTable              - a string value.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _Unknown_Processor($sTable)
	Local $Region = "Region - Unknown Processor", $sReturn, $hQuery, $aReturn

	_Log($Region & ",Global,Activated Unknown Processor")

	Switch $sTable
		Case 	$aTables[$iDevices][$iName]
				$sReturn = _SQLite_Exec($hDB, "UPDATE " & $sTable & " SET " & $sG_CRegStat&"='"&$sG_Unknown&"' WHERE "&$sG_Instance&"!='"&$dNow&"';")
				If @error Then
					If $bError Then _Log($Region & ", Error, SQLite Execution failed to update: Unknowns in table: " & $sTable & ". Returned: " & $sReturn)
					If $bError Then _Log($Region & ", Error,SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
					Return "Error"
				Else
					If $bVerbose Then _Log($Region & ", Info, Successfully updated unknown's 'CRegstat'.")
					Return "Good"
				EndIf

		Case Else
			;add code here :)
	EndSwitch

EndFunc   ;==>_Unknown_Processor


;Remove non "SEP", "CSF" and blanks entrys
; #FUNCTION# ====================================================================================================================
; Name ..........: _Type_Processor
; Description ...:
; Syntax ........: _Type_Processor($aArray, $sType)
; Parameters ....: $aArray              - an array of unknowns.
;                  $sType               - a string value.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _Type_Processor($aArray, $sType)
	Local $Region = "Region - Type Processor", $iRows, $y = 0, $aRemKeys, $sRemRange, $sFile, $sString, $hOpen, $sDName, $sWholeFile, $aPotentialDupes
	_Log($Region & ", Global, Type processor activated.")

	If $bVerbose Then _Log($Region & ", Info, Of type: 'Phone'.")
	$iRows = UBound($aArray)

	Local $aRemKeys[$iRows + 1] ; + 1 to ensure we don' run out of space from index 0 being the ubound count

	For $x = 0 To $iRows - 1
		$sType = StringLeft($aArray[$x], 3)
		Select
			Case StringInStr($sType, "SEP")
			Case StringInStr($sType, "CSF")
			Case Else
				If $y = 0 Then
					If $bVerbose Then _Log($Region & ", Info, Purging the following array lines:")
					If $bVerbose Then _Log($Region & ", Info, " & $aArray[$x] & @CRLF & "Index: " & $x)
				EndIf
				$aRemKeys[$y + 1] = String($x) ;  string this value else '0's maybe be false positives for blanks in the next section "RemKey Removal Range"
				$y += 1
				$aRemKeys[0] = $y ;This is done for the _ArrayDelete() later on
		EndSelect
	Next

	For $x = 0 To $iRows - 1
		If $aRemKeys[$x] = "" Then
			$sRemRange = $x & "-" & $iRows - 1
			If $bVerbose Then _Log($Region & ", Info, RemKey Removal Range: " & $sRemRange)
			ExitLoop
		EndIf
	Next

	_ArrayDelete($aRemKeys, $sRemRange)

	Return $aRemKeys

EndFunc   ;==>_Type_Processor

; #FUNCTION# ====================================================================================================================
; Name ..........: _Purge_Processor
; Description ...:
; Syntax ........: _Purge_Processor(Byref $aArray, Byref $aRemKeys)
; Parameters ....: $aArray              - [in/out] an array of unknowns.
;                  $aRemKeys            - [in/out] an array of unknowns.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _Purge_Processor(ByRef $aArray, ByRef $aRemKeys)
	Local $Region = "Region - Purge Processor"
	_Log($Region & ", Global, Purge Processor Activated")
	If IsArray($aArray) Then
		If $bVerbose Then _Log($Region & ", Info, Removing non-Cisco Phone devices. Dev Count: " & UBound($aArray))
		If Not _ArrayDelete($aArray, $aRemKeys) Then
			If $bError Then _Log($Region & ", Error, RemKeys: " & @error)
			Return "Error"
		EndIf
		If $bVerbose Then _Log($Region & ", Info, Removed non-Cisco Phone devices. Dev Count: " & UBound($aArray))

		If $bDEBUG Then _DebugArrayDisplay($aArray, "Purging aPhones")
	Else
		Return 'Error - Not Array'
	EndIf

EndFunc   ;==>_Purge_Processor

; #FUNCTION# ====================================================================================================================
; Name ..........: _SQL_Dev_Processor
; Description ...:
; Syntax ........: _SQL_Dev_Processor($sTable, $sHeaders, $aContent)
; Parameters ....: $sTable              - a string value.
;                  $sHeaders            - a string value.
;                  $aContent            - an array of unknowns.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _SQL_Dev_Processor($sTable, $sHeaders, $aContent)
	Local $Region = "Region - SQL Dev Processor", $aList, $fName, $iFileCount, $iReturn, $aGetTable, $iRows, $iCols, $sReturn
	Static $iCounter = 1
	_Log($Region & ", Global, Activated SQL Dev processor")

	_Log($Region & ", Global, Beginning device add to DB for next batch.")
	$iCounter = +1
	$sReturn = _SQLite_Exec($hDB, "BEGIN TRANSACTION") ; Transactional action will increase speed
	If @error Then
		If $bError Then _Log($Region & ", Error, SQLite Execution failed to 'BEGIN TRANSACTION': Returned: " & $sReturn)
		If $bError Then _Log($Region & ", Error, Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
		Return "Error"
	Else
		If $bVerbose Then _Log($Region & ", Info, SQLite Execution successful 'BEGIN TRANSACTION'")
	EndIf


	For $i = 0 To UBound($aContent) - 1
		$sReturn = _SQLaddDev($sTable, $aContent[$i][0], $aContent[$i][1], $aContent[$i][2], $aContent[$i][3])
		Select
			Case $sReturn = $SQLITE_OK
				If $bVerbose Then _Log($Region &",Info, Successfully added device: "&$aContent[$i][0])

			Case $sReturn = "Dupe"
				If $bVerbose Then _Log($Region &",Info, Skipped dupe device: "&$aContent[$i][0])

			Case $sReturn = "Error"
				If $bDEBUG Then _Log($Region &",Error, Failed to successfully add device: "&$aContent[$i][0])

		EndSelect
	Next

	$sReturn = _SQLite_Exec($hDB, "COMMIT TRANSACTION")
	If @error Then
		If $bError Then _Log($Region & ", Error, SQLite Execution failed to 'COMMIT TRANSACTION': Returned: " & $sReturn)
		If $bError Then _Log($Region & ", Error, Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
		Return "Error"
	Else
		If $bVerbose Then _Log($Region & ", Info, SQLite Execution successful 'COMMIT TRANSACTION'")
	EndIf
	_Log($Region & ", Global, Add to DB complete.")

	; Query
#cs
	$iReturn = _SQLite_GetTable2d($hDB, "SELECT * FROM status;", $aGetTable, $iRows, $iCols)
	If $iReturn = $SQLITE_OK Then
		If $bVerbose Then _SQLite_Display2DResult($aGetTable)
		If @error Then
			If $bError Then _Log($Region & ", Error, SQL Display Table status")
			If $bError Then _Log($Region & ", Error, Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
		EndIf

	Else
		If $bError Then _Log($Region & ", Error, SQL Get Table status")
		If $bError Then _Log($Region & ", Error, SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
	EndIf
#ce
EndFunc   ;==>_SQL_Dev_Processor

; #FUNCTION# ====================================================================================================================
; Name ..........: _SQL_BnB_Processor
; Description ...:
; Syntax ........: _SQL_BnB_Processor($sTable, $sIndex, $aArray)
; Parameters ....: $sTable              - a string value.
;                  $sIndex              - a string value.
;                  $aArray              - an array of unknowns.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _SQL_BnB_Processor($sTable, $sIndex, $aArray)
	Local $Region = "Region - SQL BnB Processor", $sReturn = False, $bInstanceReturn = False
	_Log($Region & ", Global, Activated SQL BnB processor")

	_Log($Region & ", Global, Beginning device add to DB for table: " & $sTable & ".")
	_SQLite_Exec($hDB, "BEGIN TRANSACTION") ; Transactional action will increase speed
	If @error Then
		If $bError Then _Log($Region & ", Error, Region - BnB Processor: SQLite Execution failed to 'BEGIN TRANSACTION': Returned: " & $sReturn)
		If $bError Then _Log($Region & ", Error, SQLite Error Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
		Return "Error"
	Else
		If $bVerbose Then _Log($Region & ", Info, SQLite Execution successful 'BEGIN TRANSACTION'")
	EndIf

	If $bDEBUG Then _DebugArrayDisplay($aArray, $sIndex)

	If $bVerbose Then _Log($Region & ",Info, Sending table:"&$sTable&" & index:"&$sIndex&" to BnB.")

	For $sEntry In $aArray
		$sReturn = _SQLaddBnB($sTable, $sIndex, $sEntry)
		If $sReturn = "Error"  Then
				If $bError Then _Log($Region &", Error, BnB error returned. Failed table write in: "&$sTable&" at Index: "&$sIndex&", for etnry:"&$sEntry&", $bInstanceReturn="&$sReturn&".")
				$bInstanceReturn = $sReturn
		EndIf
	Next

	_SQLite_Exec($hDB, "COMMIT TRANSACTION;")
	_Log($Region & ", Global, Add to DB complete.")

	If $sTable = $aTables[$iMod][$iName] Or $sTable = $aTables[$iBuilt][$iName] Then
		_Log($Region & ", Global, Verify BnB entries.")
		For $sEntry In $aArray
			$sReturn = _SQLquery($sTable, $sIndex, $sEntry)
			If $sReturn = "Error"  Then
					If $bError Then _Log($Region &", Error, CheckBnB error returned. Failed to verify write in table: "&$sTable&" at Index: "&$sIndex&", for entry:"&$sEntry&", $bInstanceReturn="&$sReturn&".")
			EndIf
		Next
		_Log($Region & ", Global, Completed BnB verification.")
	EndIf


	If @error Then
		If $bError Then _Log($Region & ", Error,  SQLite Execution failed to 'COMMIT TRANSACTION': Returned: " & $sReturn)
		If $bError Then _Log($Region & ", Error, Code: " & _SQLite_ErrCode() & " Error Message: " & _SQLite_ErrMsg())
		Return "Error"
	Else
		If $bVerbose Then _Log($Region & ", Info,  SQLite Execution successful 'COMMIT TRANSACTION'")
	EndIf

	If $bInstanceReturn = "Error" Then
		If $bError Then _Log($Region &", Error, Returning:$bInstanceReturn="&$bInstanceReturn&".")
		Return $bInstanceReturn
	Elseif $sReturn = "Error" Then
		If $bError Then _Log($Region &", Error, Returning:$sReturn="&$sReturn&".")
		Return $sReturn
	Else
		If $bVerbose Then _Log($Region &", Info, Returning:$sReturn="&$sReturn&".")
		Return "Good"
	EndIf

EndFunc   ;==>_SQL_BnB_Processor

Func _Instance_Processor()
	Local $Region = "Region - SQL BnB Processor", $sReturn = False, $bInstanceReturn = False
	_Log($Region & ", Global, Activated SQL BnB processor")

EndFunc

#EndRegion - Processors

#Region - Arrays

; #FUNCTION# ====================================================================================================================
; Name ..........: _ArrayCleanup
; Description ...: Remove columns and rows from script. $iLeft = how many columns to remove from left. $iRight... etc
; Syntax ........: _ArrayCleanup()
; Parameters ....: $iLeft, $iRight, $iTop, $iBottom
; Return values .: $aArray
; Author ........: DJ Thornton
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _ArrayCleanup($aArray, $iLeft, $iRight = Null, $iTop = Null, $iBottom = Null)
	Local $Region = "Region - Array Cleanup", $iCols, $iRows, $sError, $sRange
	_Log($Region & ", Global, ArrayCleanup Activated.")

	If $bDEBUG Then _DebugArrayDisplay($aArray, "$aArray before cleanup")

	$iCols = UBound($aArray, $UBOUND_COLUMNS)
	If $bVerbose Then _Log($Region & ", Info, Columns: " & $iCols)

	$iRows = UBound($aArray, $UBOUND_ROWS)
	If $bVerbose Then _Log($Region & ", Info, Rows: " & $iRows)

	If $iLeft <> Null Then
		;Validate there are enough columns
		If $iCols <= $iLeft Then
			$sError = "Error Col - Insufficient deminsions for request"
			_Log($sError)
			Return $sError
		EndIf

		For $i = 1 To $iLeft
			If $bVerbose Then _Log($Region & ", Info, Deleting column - 0")
			_ArrayColDelete($aArray, 0, True)
		Next

		$iCols = UBound($aArray, $UBOUND_COLUMNS)
		If $bVerbose Then _Log($Region & ", Info, Columns: " & $iCols)
	EndIf

	If $iRight <> Null Then
		If $iCols <= $iRight Then
			$sError = "Error Col - Insufficient deminsions for request"
			_Log($sError)
			Return $sError
		EndIf

		For $i = 1 To $iRight
			If $bVerbose Then _Log($Region & ", Info, Deleting column - " & UBound($aArray, $UBOUND_COLUMNS) - 1)
			_ArrayColDelete($aArray, UBound($aArray, $UBOUND_COLUMNS) - 1, True)
		Next

		$iCols = UBound($aArray, $UBOUND_COLUMNS)
		If $bVerbose Then _Log($Region & ", Info, Columns: " & $iCols)
	EndIf

	If $iTop <> Null Then
		If $iRows <= $iTop Then
			$sError = "Error Row - Insufficient deminsions for request"
			_Log($sError)
			Return $sError
		EndIf

		$sRange = "0-" & String($iTop - 1)
		If $bVerbose Then _Log($Region & ", Info, Deleting rows - " & $sRange)
		_ArrayDelete($aArray, $sRange)

		$iRows = UBound($aArray, $UBOUND_ROWS)
		If $bVerbose Then _Log($Region & ", Info, Rows: " & $iRows)
	EndIf

	If $iBottom <> Null Then
		If $iRows <= $iBottom Then
			$sError = "Error Row - Insufficient deminsions for request"
			_Log($sError)
			Return $sError
		EndIf

		$sRange = String($iRows) - String($iBottom) & "-" & String($iRows) - 1
		If $bVerbose Then _Log($Region & ", Info, Deleting rows - " & $sRange)
		_ArrayDelete($aArray, $sRange)

		$iRows = UBound($aArray, $UBOUND_ROWS)
		If $bVerbose Then _Log($Region & ", Info, Rows: " & $iRows)

	EndIf

	If $bDEBUG Then _DebugArrayDisplay($aArray, "$aArray after cleanup")

	If $bVerbose Then _Log($Region & ", Info, Array dimenstions: " & UBound($aArray, $UBOUND_DIMENSIONS))
	Return $aArray
EndFunc   ;==>_ArrayCleanup

#EndRegion - Arrays

#Region - Misc

; #FUNCTION# ====================================================================================================================
; Name ..........: _Batch
; Description ...:
; Syntax ........: _Batch($sTable, $sIndex)
; Parameters ....: $sTable              - a string value.
;                  $sIndex              - a string value.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _Batch($sTable, $sIndex)
	Local $Region = "Region - Batch", $y = 0, $iRows, $BatchCount = 1, $iBatchCount, $sBatch, $sReturn, $aArray, $iCols, $sRtnProcessor
	_Log($Region & ", Global, Activated Batch")

	$sReturn = _SQLite_GetTable($hDB, "SELECT " & $sIndex & " FROM " & $sTable & ";", $aArray, $iRows, $iCols)
	If $sReturn = $SQLITE_OK Then
		_Log($Region & ", Global, Rows: " & $iRows &  "	-	Columns: " & $iCols & @CRLF)
		_ArrayDelete($aArray, "0-1")

		If $bDEBUG Then _DebugArrayDisplay($aArray)

	Else
		_Log($Region & ", Error, SQLite Error: " & $sReturn, _SQLite_ErrMsg())
		Return 'Error'
	EndIf


	If $iRows < $iDevCount Then ;If less than the predefined device maximum (ie: 500) then set to only 1 batch; Else find out how many batches are necessary
		Local $aBatches[1]
		If $bVerbose Then _Log($Region & ", Info,Batch less than " & $iDevCount & " Created 1.")
	Else
		$iBatchCount = Ceiling($iRows / $iDevCount)
		Local $aBatches[$iBatchCount]
		If $bVerbose Then _Log($Region & ", Info, Calculated: " & $iBatchCount & " batches.")
	EndIf

	For $b = 0 To $iBatchCount - 1 ;take the entire array of phones and break it into batches defined by the $iDevCount variable. Default = 500
		If $bVerbose Then _Log($Region & ", Info,Creating batch: " & $b & " of " & $iBatchCount - 1)
		For $i = 0 To $iDevCount - 1
			If $i = 0 Then
				If $bVerbose Then _Log($Region & ", Info, Adding entry: " & $aArray[$y] & " from Array row: " & $y & " to batch row: " & $b)
				$aBatches[$b] = $aArray[$y] ;$y utilized to keep track of array position during loop
			ElseIf $i = $iDevCount - 1 Or $y = $iRows - 1 Then
				$aBatches[$b] &= ',' & $aArray[$y]
				$y += 1
				ExitLoop
			Else
				$aBatches[$b] &= ',' & $aArray[$y]
			EndIf
			$y += 1
		Next
		If $bVerbose Then _Log($Region & ", Info,Batch: " & $BatchCount & ": " & $aBatches[$b])
		$BatchCount += 1
		If $y = $iRows - 1 Then ExitLoop
	Next

	$aArray = Null

	If $bDEBUG Then _DebugArrayDisplay($aBatches, 'Batches in function')

	$sRtnProcessor = _SQL_BnB_Processor($aTables[$iBatch][$iName], $aTables[$iBatch][$iColumns], $aBatches)
	If StringInStr("Error", $sRtnProcessor) Then
		_Log($Region & ",Error,Error in SQL " & $aTables[$iBatch][$iName] & " processor function. Terminating program to protect DB.")
		_Shutdown()
	EndIf

	Return "Good"

EndFunc   ;==>_Batch

#EndRegion - Misc

#Region - Testing

Func _Test($iScenario)
	Local $aBatches, $aPhones, $iDevCount, $fName, $sTemp = "Batch"
	Local $Region = "Region - Iniate Gather", $aPhones, $aBatches, $sRtnProcessor, $sTemp, $sResults, $aArray
	;$iScenario (s) 1 = _ListPhone_Processor(); 2 = _Batch(); 3 = _SelectCMDevices(); 4 = _Startup(); 5 = _SQL_Dev_Processor(); 6 = _Shutdown()
	Select
		Case $iScenario = 1
		Case $iScenario = 2
		Case $iScenario = 3

			$iTotalListCount = 12000
			For $i = 1 To 25
				$fName = $sFilerDir & "\rawdata_" & $i & ".xml"
				_SelectCMDevices_Processor($fName, $i) ; Send the batch number for the naming process
			Next

		Case $iScenario = 4
		Case $iScenario = 5
			_Startup()
			$sRtnProcessor = _SQL_Dev_Processor($aTables[$iDevices][$iName], $aTables[$iDevices][$iColumns], $aArray)
			If StringInStr("Error", $sRtnProcessor) Then
				_Log($Region & ",Error ,Error in SQL" & $aTables[$iStatus][$iName] & " processor function. Terminating program to protect DB.")
				_Shutdown()
			EndIf

			_Shutdown()
		Case $iScenario = 6

	EndSelect

EndFunc   ;==>_Test

#EndRegion - Testing
