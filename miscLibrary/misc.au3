#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here

#include <File.au3>
#include <Array.au3>
#include <Date.au3>
#include <Debug.au3>
#include <Constants.au3>

#Region - Misc
Global	$sLogFile, $sErrorLog

Func _Filer($sFiler, $bWrite, $sContent, $bAppend = False)
	Local $Region = "Region - Filer", $hOpen, $objReturn, $iRows, $sString ;Filer will be used to hold SOAP returns and swap data around

	_Log($Region&", Info,Filer activated.")
	If $bWrite Then

		If $bAppend Then
			_Log($Region&", Info,Filer - append write mode")
			$hOpen = FileOpen($sFiler, 9) ; Open for append and path creation = 1 + 9
		Else
			_Log($Region&", Info,Filer - overwrite mode")
			$hOpen = FileOpen($sFiler, 10) ; Open for overwrite and path creation = 2 + 8.
		EndIf

		If @error Then
			_Log($Region&", Error, Error opening: "&$sFiler )
			Return 'Error - Temp1 Locked'
		EndIf

		Select
			Case IsString($sContent)
				If Not FileWrite($hOpen, $sContent) Then
					_Log($Region&", Error, Error opening: "&$sFiler )
					Return 'Error - WFail'
				EndIf
			Case IsArray($sContent)
				If Not _FileWriteFromArray($hOpen, $sContent, Default, Default, ',') Then
					_Log($Region&", Error, Error opening: "&$sFiler )
					Return 'Error - WFail'
				EndIf
		EndSelect

		_Log($Region&", Info,File write to: '" & $sFiler & "'." )

		FileClose($hOpen)

		Return $sFiler

	Else
		_Log($Region&", Info,Filer - Opening: "&$sFiler)
		$hOpen = FileOpen($sFiler, 0)

		Select
			Case $sContent = 'Array'
				_Log($Region&", Info,Reading to array")
				_FileReadToArray($hOpen, $objReturn, 1, ',')
			Case $sContent = 'String'
				_Log($Region&", Info,Reading to string")
				 $objReturn = FileRead($hOpen)
			Case Else ;Search
				_Log($Region&", Info,Searching for: "&$sContent)
				$sString = FileRead($hOpen)
				If StringInStr($sString, $sContent) Then
					$objReturn = $sString
				Else
					$objReturn = 'Clean'
				EndIf
		EndSelect

		FileClose($hOpen)
		Return $objReturn

	EndIf


EndFunc   ;==>_Filer

Func _TimeStamp()
	Return '['&_NowCalc()&'] ,'
EndFunc   ;==>_TimeStamp

Func _Log($sContent, $fILE = '.\LogFile.log', $bMsgBox = False, $iMsgBoxIcon = False)
	Local $Region = "Region - Log", $hFile
	If $sLogFile <> "" Then $fILE = $sLogFile
	If Not FileExists($fILE) Then
		If Not _FileCreate($fILE) Then
			_Log($Region&", Error , Error creating "&$fILE&": "&@error)
			Return 'Log Error'
		EndIf
	EndIf

	If $bMsgBox Then
		If $iMsgBoxIcon = False Then $iMsgBoxIcon = $MB_ICONINFORMATION
		ConsoleWrite(_TimeStamp()&$sContent&@CRLF)
		FileWriteLine($fILE, _TimeStamp()&$sContent)
	Else
		ConsoleWrite(_TimeStamp()&$sContent&@CRLF)
		FileWriteLine($fILE, _TimeStamp()&$sContent)
	EndIf

EndFunc

; Get timestamp for input datetime (or current datetime).
Func _GetEpochTime($sDate = 0);Date Format: 2013/01/01 00:00:00 ~ Year/Mo/Da HH:MM:SS

    Local $aSysTimeInfo = _Date_Time_GetTimeZoneInformation()
    Local $utcTime = ""

    If Not $sDate Then $sDate = _NowCalc()

    If Int(StringLeft($sDate, 4)) < 1970 Then Return ""

    If $aSysTimeInfo[0] = 2 Then ; if daylight saving time is active
        $utcTime = _DateAdd('n', $aSysTimeInfo[1] + $aSysTimeInfo[7], $sDate) ; account for time zone and daylight saving time
    Else
        $utcTime = _DateAdd('n', $aSysTimeInfo[1], $sDate) ; account for time zone
    EndIf

    Return _DateDiff('s', "1970/01/01 00:00:00", $utcTime)
EndFunc   ;==>_GetUnixTime

;$blTrim: Year in short format and no seconds.
Func _GetDate_fromEpochTime($iUnixTime, $iReturnLocal = True)
    Local $aRet = 0, $aDate = 0
    Local $aMonthNumberAbbrev[13] = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    Local $timeAdj = 0
    If Not $iReturnLocal Then
        Local $aSysTimeInfo = _Date_Time_GetTimeZoneInformation()
        Local $timeAdj = $aSysTimeInfo[1] * 60
        If $aSysTimeInfo[0] = 2 Then $timeAdj += $aSysTimeInfo[7] * 60
    EndIf

    $aRet = DllCall("msvcrt.dll", "str:cdecl", "ctime", "int*", $iUnixTime + $timeAdj )

    If @error Or Not $aRet[0] Then Return ""

    $aDate = StringSplit(StringTrimRight($aRet[0], 1), " ", 2)

    Return $aDate[4] & "/" & StringFormat("%.2d", _ArraySearch($aMonthNumberAbbrev, $aDate[1])) & "/" & $aDate[2] & " " & $aDate[3]
EndFunc   ;==>_GetUnixDate
#EndRegion - Misc
